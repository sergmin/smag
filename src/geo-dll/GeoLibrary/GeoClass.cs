﻿using System;

/// <summary>
/// Здесь определены используемые структуры, а также статический класс с необходимым функционалом.
/// </summary>
namespace GeoStars
{
    /// <summary>
    /// Класс перевода геодезических координат в геоцентрические и обратно.
    /// </summary>
    /// <remarks>Здесь содержатся методы для пересчёта координат (LatLonHgt2Xyz и Xyz2LatLonHgt),
	/// методы для вычисления расстояний по поверхности Земли (GetDistanceCurve), и по прямой (GetDistanceStraight),
    /// и методы для перевода градусов в радианы (Deg2Rads) и обратно (Rad2Degs).</remarks>
    public static class Geo
    {
        /// <summary>
        /// Средний радиус Земли в метрах.
        /// </summary>
        private const double EARTH_RADIUS = 6371009;

        /// <summary>
        /// Коэффициент перевода из десятичных градусов в радианы.
        /// </summary>
        private const double DEG_TO_RAD = Math.PI / 180;

        /// <summary>
        /// Коэффициент перевода из радиан в десятичные градусы.
        /// </summary>
        private const double RAD_TO_DEG = 180 / Math.PI;

        /// <summary>
        /// Структура с информацией об используемом эллипсе.
        /// </summary>
        private struct EllipseInfo
        {
            /// <summary>
            /// Большая полуось эллипса в метрах.
            /// </summary>
            public double AxisMajor { get; set; }

            /// <summary>
            /// Сжатие эллипса в метрах.
            /// </summary>
            public double Flattening { get; set; }

            /// <summary>
            /// Малая полуось эллипса в метрах.
            /// </summary>
            public double AxisMinor { get; set; }

            /// <summary>
            /// Эсцентриситет эллипса.
            /// </summary>
            public double Eccentricity { get; set; }

            /// <summary>
            /// Второй эксцентриситет эллипса.
            /// </summary>
            public double EccentricityPrime { get; set; }
        }

        /// <summary>
        /// Вычисляет данные эллипса, необходимого для вычислений.
        /// </summary>
        /// <param name="info">Тип эллипса из перечисления Ellipses.</param>
		/// <returns>Заполненная структура с информацией об эллипсе.</returns>
        private static EllipseInfo GetEllipsoid(Ellipse info)
        {
            EllipseInfo temp = new EllipseInfo(); // структура с информацией об эллипсе

            switch (info)
            {
                case Ellipse.WGS84:
                    {
                        temp.AxisMajor = 6378137;
                        temp.Flattening = 1 / 298.257223563;
                        temp.AxisMinor = temp.AxisMajor * (1 - temp.Flattening);
                        temp.Eccentricity = (Math.Pow(temp.AxisMajor, 2) - Math.Pow(temp.AxisMinor, 2)) / Math.Pow(temp.AxisMajor, 2);
                        temp.EccentricityPrime = (Math.Pow(temp.AxisMajor, 2) - Math.Pow(temp.AxisMinor, 2)) / Math.Pow(temp.AxisMinor, 2);
                        break;
                    }
                case Ellipse.Airy1830:
                    {
                        temp.AxisMajor = 6377563.396;
                        temp.Flattening = 1 / 299.3249646;
                        temp.AxisMinor = temp.AxisMajor * (1 - temp.Flattening);
                        temp.Eccentricity = (Math.Pow(temp.AxisMajor, 2) - Math.Pow(temp.AxisMinor, 2)) / Math.Pow(temp.AxisMajor, 2);
                        temp.EccentricityPrime = (Math.Pow(temp.AxisMajor, 2) - Math.Pow(temp.AxisMinor, 2)) / Math.Pow(temp.AxisMinor, 2);
                        break;
                    }
                case Ellipse.Krasovski:
                    {
                        temp.AxisMajor = 6378245;
                        temp.Flattening = 1 / 298.3;
                        temp.AxisMinor = temp.AxisMajor * (1 - temp.Flattening);
                        temp.Eccentricity = (Math.Pow(temp.AxisMajor, 2) - Math.Pow(temp.AxisMinor, 2)) / Math.Pow(temp.AxisMajor, 2);
                        temp.EccentricityPrime = (Math.Pow(temp.AxisMajor, 2) - Math.Pow(temp.AxisMinor, 2)) / Math.Pow(temp.AxisMinor, 2);
                        break;
                    }
            }

            return temp;
        }

        /// <summary>
        /// Пересчёт координат из широты/долготы/высоты в XYZ.
        /// </summary>
        /// <remarks>Широта и долгота должны быть в радианах, высота в метрах. Для перевода градусов в радианы есть метод Deg2Rads.</remarks>
        /// <param name="latlon">Координаты LLH, которые необходимо пересчитать.</param>
        /// <returns>Координаты XYZ в метрах.</returns>
        /// <seealso cref="Deg2Rads(DegMinSec, Direction)"/>
        public static XYZ LatLonHgt2Xyz(LatLonHgt latlon)
        {
            EllipseInfo info_ellips = GetEllipsoid(Ellipse.WGS84); // получение информации об используемом эллипсе

            double clat = Math.Cos(latlon.Latitude); // косинус широты
            double slat = Math.Sin(latlon.Latitude); // синус широты

            double curve = info_ellips.AxisMajor / Math.Sqrt(1 - info_ellips.Eccentricity * Math.Pow(slat, 2)); // радиус кривизны первого вертикала

            XYZ xyz = new XYZ // вычисление координат XYZ
            {
                X = (curve + latlon.Height) * clat * Math.Cos(latlon.Longitude),
                Y = (curve + latlon.Height) * clat * Math.Sin(latlon.Longitude),
                Z = (curve * (1 - info_ellips.Eccentricity) + latlon.Height) * Math.Sin(latlon.Latitude)
            };

            return xyz;
        }

        /// <summary>
        /// Пересчёт координат из XYZ в широту/долготу/высоту. 
        /// </summary>
        /// <remarks>Координаты XYZ должны быть в метрах. 
        /// Для перевода радиан в градусы есть метод Rad2Degs.</remarks>
        /// <param name="xyz">Координаты XYZ, которые необходимо пересчитать.</param>
        /// <returns>Координаты LLH (широта и высота в радианах, высота в метрах).</returns>
        /// <seealso cref="Rads2Deg(double)"/>
        public static LatLonHgt Xyz2LatLonHgt(XYZ xyz)
        {
            EllipseInfo info_ellips = GetEllipsoid(Ellipse.WGS84); // получение информации об используемом эллипсе

            double sign = 1; // знак, определяющий высоту

            double p = Math.Sqrt(Math.Pow(xyz.X, 2) + Math.Pow(xyz.Y, 2)); // расстояние от оси вращения эллипсоида
            double u = Math.Atan((xyz.Z / p) * (info_ellips.AxisMajor / info_ellips.AxisMinor)); // предварительная оценка широты
            double su = Math.Sin(u); // синус оценки широты
            double cu = Math.Cos(u); // косинус оценки широты

            double temp_lat = Math.Atan((xyz.Z + info_ellips.EccentricityPrime * info_ellips.AxisMinor * Math.Pow(su, 3)) /
                (p - info_ellips.Eccentricity * info_ellips.AxisMajor * Math.Pow(cu, 3))); // рассчитанная широта

            double u_prime = Math.Atan((1 - info_ellips.Flattening) * Math.Tan(temp_lat)); // приведенная широта

            double cu_prime = p - info_ellips.AxisMajor * Math.Cos(u_prime); // косинус приведенной широты
            if (cu_prime < 0)
                sign = -1;

            double t1 = xyz.Z - info_ellips.AxisMinor * Math.Sin(u_prime); // параметр для расчета высоты
			
			double temp_lon = Math.Atan2(xyz.Y, xyz.X); // расчитанная долгота

			if (xyz.X == 0 & xyz.Y == 0) // обработка исключения (X == Y == 0)
			{
				temp_lon = Math.PI;
			}

            LatLonHgt latlon = new LatLonHgt // вычисление координат
            {
                Latitude = temp_lat,
                Longitude = temp_lon,
                Height = sign * Math.Sqrt(Math.Pow(cu_prime, 2) + Math.Pow(t1, 2))
            };

            return latlon;
        }

        /// <summary>
        /// Получение расстояния между двумя точками с учётом кривизны поверхности Земли.
        /// </summary>
        /// <remarks>Метод рассчитывает расстояние с учётом кривизны планеты по ортодромии. 
        /// В отличие от метода GetDistanceStraight, в расчётах не используется высота, 
        /// поэтому можно не применять обработку исключений к свойству LLH.Height входных параметров. 
        /// Для перевода из градусов в радианы есть метод Deg2Rads.</remarks>
        /// <param name="point1">Координаты LLH первой точки (широта и долгота в радианах, высота любая).</param>
        /// <param name="point2">Координаты LLH второй точки (широта и долгота в радианах, высота любая).</param>
        /// <returns>Расстояние между двумя указанными точками в метрах.</returns>
        /// <seealso cref="Deg2Rads(DegMinSec, Direction)"/>
        /// <seealso cref="GetDistanceStraight(LatLonHgt, LatLonHgt)"/>
        public static double GetDistanceCurve(LatLonHgt point1, LatLonHgt point2)
        {
            double slat1 = Math.Sin(point1.Latitude); // синус широты первой точки
            double slat2 = Math.Sin(point2.Latitude); // синус широты второй точки

            double clat1 = Math.Cos(point1.Latitude); // косинус широты первой точки
            double clat2 = Math.Cos(point2.Latitude); // косинус широты второй точки
            double clon_diff = Math.Cos(point1.Longitude - point2.Longitude); // косинус разности долгот первой и второй точки

            double distance = Math.Acos(slat1 * slat2 + clat1 * clat2 * clon_diff); // угловая длина ортодромии

            return distance * EARTH_RADIUS; // итоговое расстояние есть произведение угловой длины ортодромии на средний радиус планеты
        }

        /// <summary>
        /// Получение расстояния между двумя точками в околоземном пространстве по прямой.
        /// </summary>
        /// <remarks>Метод рассчитывает расстояние по прямой в околоземном пространстве. 
        /// Таким образом, в отличие от метода GetDistanceCurve в этом методе требуется высота объектов над поверхностью Земли.</remarks>
        /// <param name="point1">Координаты LLH первой точки (широта и долгота в радианах, высота в метрах).</param>
        /// <param name="point2">Координаты LLH второй точки (широта и долгота в радианах, высота в метрах).</param>
        /// <returns>Расстояние между двумя точками в околоземном пространстве по прямой в метрах.</returns>
        /// <seealso cref="GetDistanceCurve(LatLonHgt, LatLonHgt)"/>
        public static double GetDistanceStraight(LatLonHgt point1, LatLonHgt point2)
        {
            XYZ point1Xyz = LatLonHgt2Xyz(point1); // получение XYZ-координат первой точки
            XYZ point2Xyz = LatLonHgt2Xyz(point2); // получение XYZ-координат второй точки

            // расчет квадратов разниц координат
            double xDiff = Math.Pow(point2Xyz.X - point1Xyz.X, 2); // квадрат разницы координаты Х
            double yDiff = Math.Pow(point2Xyz.Y - point1Xyz.Y, 2); // квадрат разницы координаты Y
            double zDiff = Math.Pow(point2Xyz.Z - point1Xyz.Z, 2); // квадрат разницы координаты Z

            return Math.Sqrt(xDiff + yDiff + zDiff); // расстояние рассчитывается по формуле Евклидова расстояния
        }

        /// <summary>
        /// Перевод градусов/минут/секунд в радианы.
        /// </summary>
        /// <remarks>Справка: в метод передаются по отдельности широта и долгота, каждая в своём объекте структуры DegMinSec. 
        /// Направление Direction необходимо для определения итогового знака полученного значения радиан. Оно зависит от широты и долготы объекта.
        /// Если широта северная, то передается North, либо Positive. Если южная, то South, либо Negative.
        /// Если долгота восточная, то передается East, либо Positive. Если западная, то West, либо Negative.</remarks>
        /// <param name="degs">Градусы/минуты/секунды.</param>
        /// <param name="sign">Элемент перечисления Direction, определяющий направление преобразования.</param>
        /// <returns>Полученное значение радиан.</returns>
        public static double Deg2Rads(DegMinSec degs, Direction sign)
        {
            double direction; // направление преобразования (зависит от долготы и широты, см. перечисление Direction)

            double degree = Math.Abs(degs.Degrees) + (degs.Minutes + degs.Seconds / 60) / 60; // перевод градусов/минут/секунд в десятичные градусы

            switch (sign) // определение знака преобразования
            {
                case Direction.Negative:
                case Direction.West:
                case Direction.South:
                    {
                        direction = -1.0;
                        break;
                    }
                case Direction.Positive:
                case Direction.East:
                case Direction.North:
                    {
                        direction = 1.0;
                        break;
                    }
                default:
                    {
                        direction = 1.0;
                        break;
                    }
            }

            return direction * DEG_TO_RAD * degree; // перевод градусов в радианы
        }

        /// <summary>
        /// Перевод из радианов в градусы/минуты/секунды.
        /// </summary>
        /// <remarks>В отличие от метода Deg2Rads, этот метод не требует определения направления, 
        /// поэтому дополнительные параметры не требуются.</remarks>
        /// <param name="radian">Радианы, которые необходимо перевести.</param>
        /// <returns>Градусы/минуты/секунды, упакованные в структуру.</returns>
        /// <seealso cref="Deg2Rads(DegMinSec, Direction)"/>
        public static DegMinSec Rads2Deg(double radian)
        {
            DegMinSec degs = new DegMinSec();
            double fraction; // дробная часть

            double radian_abs = Math.Abs(radian); // берем абсолютное значение радиан
            double temp = RAD_TO_DEG * radian_abs; // переводим его в десятичные градусы

            degs.Degrees = Math.Truncate(temp); // градусы - целая часть от полученных десятичных градусов

            fraction = temp - Math.Truncate(temp); // берем дробную часть
            temp = fraction * 60; // получаем десятичные минуты
            degs.Minutes = Math.Truncate(temp); // минуты - целая часть от полученных десятичных минут

            fraction = temp - Math.Truncate(temp); // берем дробную часть
            degs.Seconds = fraction * 60; // получаем секунды

            return degs;
        }
    }
}
