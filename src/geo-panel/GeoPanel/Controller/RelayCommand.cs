﻿using System;
using System.Windows.Input;

namespace GeoPanel
{
	/// <summary>
	/// Класс для использования команд
	/// </summary>
	public class RelayCommand : ICommand
	{
		/// <summary>
		/// Приватное поле,отвечающее выполнение.
		/// </summary>
		private  readonly Action<object> execute;
		/// <summary>
		/// Приватное поле,разрешающее выполнение.
		/// </summary>
		private  readonly Func<object, bool> canExecute;
		/// <summary>
		/// Свойство,обеспичивающее включения/отключения команды.
		/// </summary>
		public event EventHandler CanExecuteChanged
		{
			add { CommandManager.RequerySuggested += value; }
			remove { CommandManager.RequerySuggested -= value; }
		}


		/// <summary>
		/// Инициализация команды.
		/// </summary>
		/// <param name="exeс">Выполняемый метод команды</param>
		/// <param name="canExec">Метод разрешающий выполнение команды</param>
		public RelayCommand(Action<object> exeс, Func<object, bool> canExec = null)
		{
			execute = exeс;
			canExecute = canExec;
		}
		/// <summary>
		/// Условие разрешения выполнения действия
		/// </summary>
		/// <param name="parameter">Параметр команды</param>
		/// <returns>True - если выполнение команды разрешено</returns>
		public bool CanExecute(object parameter) => canExecute == null || canExecute(parameter);
		/// <summary>
		/// Выполнение команды.
		/// </summary>
		/// <param name="parameter">Параметр команды</param>
		public void Execute(object parameter) => execute(parameter);
	}
}
