﻿using System;
using System.IO;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Data;
using System.Globalization;
using Microsoft.Win32;
using System.Windows;
using Excel = Microsoft.Office.Interop.Excel;

namespace GeoPanel
{
	/// <summary>
	/// Класс,обеспечивающий связь View и Model.
	/// </summary>
	class Controller : INotifyPropertyChanged
	{
		/// <summary>
		/// Поле, отвечающее за форматы вывода даты.
		/// </summary>
		private readonly CultureInfo culture;
		/// <summary>
		/// Журнал программы.
		/// </summary>
		private DataTable result;
		/// <summary>
		/// Команда, которая нужна для смены языка.
		/// </summary>
		private RelayCommand changeLanguage;
		/// <summary>
		/// Команда,которая отвечает за смену языка.
		/// </summary>
		public RelayCommand ChangeLanguage => changeLanguage ??
				  (changeLanguage = new RelayCommand(obj => ChangeLang()));
		/// <summary>
		/// Поле, связанное с этой подсказкой.
		/// </summary>
		private string errorToolTip;
		/// <summary>
		/// Подсказка о вознинковении ошибки.
		/// </summary>
		public string ErrorToolTip
		{
			get => errorToolTip;
			set
			{
				errorToolTip = value;
				OnPropertyChanged();

			}
		}
		private RelayCommand outFile;
		/// <summary>
		/// Вывод в файл
		/// </summary>
		public RelayCommand OutFile => outFile ??
				  (outFile = new RelayCommand(obj => OutputToFile()));
		/// <summary>
		/// Поле,относящееся к вычислению по одному из выбранных видов.
		/// </summary>
		private RelayCommand calcResult;
		/// <summary>
		/// Поле,относящееся к вычислению множества данных.
		/// </summary>
		private RelayCommand calcManyData;
		/// <summary>
		/// Вычисление по одному из выбранных видов.
		/// </summary>
		public RelayCommand CalcResult => calcResult ?? (calcResult = new RelayCommand(obj => Calculation()));
		/// <summary>
		/// Вычисление по одному из выбранных видов.
		/// </summary>
		public RelayCommand СalcManyData => calcManyData ??
				  (calcManyData = new RelayCommand(obj => DataCalculation()));
		/// <summary>
		/// Строка для перевода GroupBox первой точки.
		/// </summary>
		public string TitleFirstCoord => ChangeFunc("coordinates of the first point", "координаты первой точки");
		/// <summary>
		/// Строка для перевода GroupBox второй точки.
		/// </summary>
		public string TitleSecondCoord => ChangeFunc("coordinates of the second point", "координаты второй точки");
		/// <summary>
		/// Строка для перевода названия режима расчёта.
		/// </summary>
		public string TypeCalc => ChangeFunc("Type of calculation", "Вид расчёта");
		/// <summary>
		/// Строка для перевода названия режимов выбора полярности долготы и широты.
		/// </summary>
		public string TitleLatAndLon => ChangeFunc("The polarity of longitude and latitude", "Полярность долготы и широты");
		/// <summary>
		/// Строка для перевода геоцентрических координат в геодезические.
		/// </summary>
		public string TitleFirstFunc => ChangeFunc(
					 "The transformation of geocentric to geodetic coordinates"
					, "Преобразование геоцентрических в геодезические координаты");
		/// <summary>
		/// Строка для перевода геодезических координат в геоцентрические.
		/// </summary>
		public string TitleFirstFuncReverse => ChangeFunc(
					 "The transformation of  geodetic to geocentric coordinates"
					, "Преобразование геодезических координат в геоцентрические");
		/// <summary>
		/// Строка для перевода второго режима расчёта.
		/// </summary>
		public string TitleSecondFunc => ChangeFunc(
					 "Calculating the distance between two objects (taking into account the curvature)",
					 "Расчёт расстояния между двумя объектами (с учётом кривизны)");
		/// <summary>
		/// Строка для перевода третьего режима расчёта.
		/// </summary>
		public string TitleThirdFunc => ChangeFunc("Calculating the straight line distance between two objects", "Расчёт расстояния по прямой между двумя объектами");
		/// <summary>
		/// Строка для перевода названия  первой вкладки таблицы.
		/// </summary>
		public string TitleFirst => ChangeFunc("First Point", "Первая точка");
		/// <summary>
		/// Строка для перевода названия  второй вкладки таблицы.
		/// </summary>
		public string TitleSecond => ChangeFunc("Second Point", "Вторая точка");
		/// <summary>
		/// Строка для перевода кнопки "Запись в файл.".
		/// </summary>
		public string TitleOutput => ChangeFunc("Output to file", "Запись в файл");
		/// <summary>
		/// Строка для перевода кнопки "Рассчитать".
		/// </summary>
		public string TitleCalc => ChangeFunc("Calculate", "Рассчитать");
		/// <summary>
		/// Строка для перевода кнопки "Сменить язык".
		/// </summary>
		public string TitleChangeLang => ChangeFunc("Change  language", "Сменить язык");
		/// <summary>
		/// Поле, отвечающее за язык.
		/// </summary>
		private bool language;
		/// <summary>
		/// Cвойство, отвечающее за язык.
		/// </summary>
		public bool Language
		{
			get => language;
			set
			{
				language = value;

				OnPropertyChanged();
				OnPropertyChanged("TitleChangeLang");
				OnPropertyChanged("TitleCalc");
				OnPropertyChanged("TitleOutput");
				OnPropertyChanged("TitleFirst");
				OnPropertyChanged("TitleSecond");
				OnPropertyChanged("TitleFirstFunc");
				OnPropertyChanged("TitleFirstFuncReverse");
				OnPropertyChanged("TitleSecondFunc");
				OnPropertyChanged("TitleThirdFunc");
				OnPropertyChanged("TypeCalc");
				OnPropertyChanged("TitleLatAndLon");
				OnPropertyChanged("TitleFirstCoord");
				OnPropertyChanged("TitleSecondCoord");
			}
		}

		/// <summary>
		/// Функция, меняющая язык.
		/// </summary>
		private void ChangeLang()
		{
			if (fPoint.Language)
			{

				Language = false;
				fPoint.Language = false;
				sPoint.Language = false;
			}
			else
			{
				Language = true;
				fPoint.Language = true;
				sPoint.Language = true;
			}
		}
		/// <summary>
		/// Функция меняюшая язык.
		/// </summary>
		public string ChangeFunc(string textRus, string textEng) => Language ? textRus : textEng;
		/// <summary>
		/// Функция,выполняющая рассчёт по одному из выбранных режимов.
		/// </summary>
		private void Calculation()
		{
			if (Result.Rows.Count < 10000)
			{
				if (ErrorToolTip.Length < 1)
				{
					if (TransformCoord)
					{
						if (!Language)
						{
							Result.Rows.Add(DateTime.Now.ToString("G", culture), FPoint.ToString(), null, " Пересчёт координат", FPoint.ToStringRus());
							Result.Rows.Add(DateTime.Now.ToString("G", culture), null, SPoint.ToString(), " Пересчёт координат", SPoint.ToStringRus());
						}
						else
						{
							Result.Rows.Add(DateTime.Now.ToString("G", culture), FPoint.ToString(), null, "Сonversion of coordinates", FPoint.ToStringEng());
							Result.Rows.Add(DateTime.Now.ToString("G", culture), null, SPoint.ToString(), "Сonversion of coordinates", SPoint.ToStringEng());
						}
					}
					if (TransformCoordReverse)
					{
						if (!Language)
						{
							Result.Rows.Add(DateTime.Now.ToString("G", culture), FPoint.ToStringRus(), null, " Пересчёт координат", FPoint.ToString());
							Result.Rows.Add(DateTime.Now.ToString("G", culture), null, SPoint.ToStringRus(), " Пересчёт координат", SPoint.ToString());
						}
						else
						{
							Result.Rows.Add(DateTime.Now.ToString("G", culture), FPoint.ToStringEng(), null, "Сonversion of coordinates", FPoint.ToString());
							Result.Rows.Add(DateTime.Now.ToString("G", culture), null, SPoint.ToStringEng(), "Сonversion of coordinates", SPoint.ToString());
						}
					}

					if (DistStraight)
					{
						if (!Language)
						{
							Result.Rows.Add(DateTime.Now.ToString("G", culture), FPoint.ToString(), SPoint.ToString(), "Расчёт расстояния по прямой", GeoStars.Geo.GetDistanceStraight(FPoint.Llh, SPoint.Llh) + " м");
						}
						else
						{
							Result.Rows.Add(DateTime.Now.ToString("G", culture), FPoint.ToString(), SPoint.ToString(), "Calculating the distance in a straight line", GeoStars.Geo.GetDistanceStraight(FPoint.Llh, SPoint.Llh) + " m");
						}
					}
					if (DistСurvature)
					{
						if (!Language)
						{
							Result.Rows.Add(DateTime.Now.ToString("G", culture), FPoint.ToString(), SPoint.ToString(), "Расчёт расстояния по дуге", GeoStars.Geo.GetDistanceCurve(FPoint.Llh, SPoint.Llh) + " м");
						}
						else
						{
							Result.Rows.Add(DateTime.Now.ToString("G", culture), FPoint.ToString(), SPoint.ToString(), "The calculation of the distance along the arc", GeoStars.Geo.GetDistanceCurve(FPoint.Llh, SPoint.Llh) + " m");
						}
					}
				}
				ErrorToolTip = "";
			}
			else
			{

				if (MessageBox.Show(
		   "Превышен лимит строк! Хотите записать данные в файл?",
		   "Сообщение",
		   MessageBoxButton.YesNo,
		   MessageBoxImage.Information,
		   MessageBoxResult.Yes,
		   MessageBoxOptions.DefaultDesktopOnly) == MessageBoxResult.Yes)
					OutputToFile();
				Result.Rows.Clear();
				Calculation();
			}
		}
		/// <summary>
		/// Функция, обеспечивающая вывод в файл.
		/// </summary>
		private void OutputToFile()
		{
			SaveFileDialog dlg = new SaveFileDialog
			{
				FileName = "Result", // Default file name
				DefaultExt = ".txt", // Default file extension
				Filter = "Текстовые документы (.txt)|*.txt" // Filter files by extension
			};
			if (dlg.ShowDialog() == true)
			{


				string path = dlg.FileName;
				using (StreamWriter sw = new StreamWriter(path, false))
				{
					bool title = true;
					int otstupi = 80;
					foreach (DataRow ch in Result.Rows)
					{

						string first = new String(' ', otstupi - ch[1].ToString().Length);
						string second = new String(' ', otstupi - ch[2].ToString().Length);
						string third = new String(' ', otstupi - ch[3].ToString().Length);
						string eight = new String(' ', 70 - ch[4].ToString().Length);
						if (title)
						{
							first = new String(' ', otstupi - Result.Columns[1].ColumnName.Length);
							second = new String(' ', otstupi - Result.Columns[2].ColumnName.Length);
							third = new String(' ', otstupi - Result.Columns[3].ColumnName.Length);
							eight = new String(' ', 70 - Result.Columns[4].ColumnName.Length);
							if (Language)
							{
								sw.WriteLine("Date and time" + first + "First point" + second + "Second point" + third + "Type of result" + eight + "Result" + "     ");
							}
							else
								sw.WriteLine("Дата и время" + first + "Первая точка" + second + "Вторая точка" + third + "Тип вычислений" + eight + "Результат" + "     ");
							title = false;
							first = new String(' ', otstupi - ch[1].ToString().Length);
							second = new String(' ', otstupi - ch[2].ToString().Length);
							third = new String(' ', otstupi - ch[3].ToString().Length);
							eight = new String(' ', 70 - ch[4].ToString().Length);
							sw.WriteLine(ch[0].ToString() + "|" + first + ch[1].ToString() + "|" + second + "|" + ch[2].ToString() + "|" + third + ch[3].ToString() + "|" + eight + ch[4].ToString() + "     ");
						}
						else
						{
							sw.WriteLine(ch[0].ToString() + "|" + first + ch[1].ToString() + "|" + second + "|" + ch[2].ToString() + "|" + third + ch[3].ToString() + "|" + eight + ch[4].ToString() + "     ");
						}
					}
				}

			}
		}
		/// <summary>
		/// Cчитаем данные из ффайла и записываем в другой.
		/// </summary>
		private void DataCalculation()
		{
			OpenFileDialog dlg = new OpenFileDialog();
			if (dlg.ShowDialog() == true)
			{
				Excel.Application ObjWorkExcel = new Excel.Application(); //открыть эксель
				Excel.Workbook ObjWorkBook = ObjWorkExcel.Workbooks.Open(dlg.FileName);
				Excel.Worksheet ObjWorkSheet = (Excel.Worksheet)ObjWorkBook.Sheets[1]; //получить 1 лист
				var lastCell = ObjWorkSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell);//1 ячейку			
				double[,] masXYZ = new double[3, lastCell.Row];
				for (int i = 0; i < lastCell.Column; i++) //по всем колонкам
					for (int j = 0; j < lastCell.Row; j++) // по всем строкам
					{

						if (j > 0)
						{
							var StringMas = double.TryParse("1,0", out _) ? ObjWorkSheet.Cells[j + 1, i + 1].Text.ToString().Replace(".", ",").Split(';') : ObjWorkSheet.Cells[j + 1, i + 1].Text.ToString().Replace(",", ".").Split(';');//считываем текст в строку	
							FPoint.Latitude = Convert.ToDouble(StringMas[1]);
							FPoint.Longitude = Convert.ToDouble(StringMas[2]);
							FPoint.Height = Convert.ToDouble(StringMas[3]);
							masXYZ[0, j] = FPoint.GeoFirst;
							masXYZ[1, j] = FPoint.GeoSecond;
							masXYZ[2, j] = FPoint.GeoThird;
						}
					}
				ObjWorkBook.Close(false); //закрыть не сохраняя
				Excel.Workbook WorkBook = ObjWorkExcel.Workbooks.Add();
				ObjWorkExcel.DisplayAlerts = false;
				Excel.Worksheet ObjCells = (Excel.Worksheet)WorkBook.Sheets[1];
				ObjCells.Cells[1, 1] = "X";
				ObjCells.Cells[1, 2] = "Y";
				ObjCells.Cells[1, 3] = "Z";
					for (int i = 1; i < 4; i++) //по всем колонкам
					for (int j = 2; j < masXYZ.GetLength(1); j++) // по всем строкам
					{
							ObjCells.Cells[j, i] = masXYZ[i - 1, j - 1];
					}
				ObjCells.Columns["A:I"].AutoFit();
		
				SaveFileDialog dialog = new SaveFileDialog();
				dialog.Filter="xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*";
				if (dialog.ShowDialog() == true)
					WorkBook.SaveAs(dialog.FileName);
				WorkBook.Close(true);
				ObjWorkExcel.Quit(); 

			}
		}
		/// <summary>
		/// Cтруктура для выборов режима расчёта.
		/// </summary>
		private ResultStruct typeResult;
		/// <summary>
		/// Первый режим расчёта,обеспечивающий преобразование геоцентрических координат.
		/// </summary>
		public bool TransformCoord
		{
			get => typeResult.TransformCoord;
			set
			{
				typeResult.TransformCoord = value;
				OnPropertyChanged();
			}
		}
		/// <summary>
		/// Первый режим расчёта,обеспечивающий преобразование геодезических координат.
		/// </summary>
		public bool TransformCoordReverse
		{
			get => typeResult.TransformCoordReverse;
			set
			{
				typeResult.TransformCoordReverse = value;
				OnPropertyChanged();
			}
		}
		/// <summary>
		/// Второй режим расчёта,обеспечивающий рассчёт расстояния с учётом кривизны Земли.
		/// </summary>
		public bool DistСurvature
		{
			get => typeResult.DistСurvature;
			set
			{
				typeResult.DistСurvature = value;
				OnPropertyChanged();
			}
		}
		/// <summary>
		/// Третий режим расчёта,обеспечивающий рассчёт расстояния по прямой.
		/// </summary>
		public bool DistStraight
		{
			get => typeResult.DistStraight;
			set
			{
				typeResult.DistStraight = value;
				OnPropertyChanged();
			}
		}

		/// <summary>
		/// Журнал с результатами
		/// </summary>
		public DataTable Result
		{
			get => result;
			set
			{
				result = value;
				OnPropertyChanged();
			}
		}
		private GeoModel fPoint;
		/// <summary>
		/// Первая точка.
		/// </summary>
		public GeoModel FPoint
		{
			get => fPoint;
			set
			{
				fPoint = value;
				OnPropertyChanged("FPoint");
			}
		}
		private GeoModel sPoint;
		/// <summary>
		/// Вторая точка.
		/// </summary>
		public GeoModel SPoint
		{
			get => sPoint;
			set
			{
				sPoint = value;
				OnPropertyChanged("SPoint");
			}
		}
		/// <summary>
		///  Cобытие, возникающее при смене значения свойства
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;
		/// <summary>
		/// Начальная инициализация объектов связанных с окном
		/// </summary>
		public Controller()
		{

			ErrorToolTip = "";
			culture = CultureInfo.CreateSpecificCulture("eu-ES");
			typeResult = new ResultStruct(true, false, false, false);
			SPoint = new GeoModel();
			FPoint = new GeoModel();
			Result = new DataTable("Результаты вычислений");
			Result.Columns.Add("Дата и время");
			Result.Columns.Add("Первая точка");
			Result.Columns.Add("Вторая точка");
			Result.Columns.Add("Тип вычислений");
			Result.Columns.Add("Результат");
		}
		/// <summary>
		/// Обработчик изменения свойства
		/// </summary>
		/// <param name="prop"> Название свойства</param>
		public void OnPropertyChanged([CallerMemberName] string prop = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
		}
	}
}
