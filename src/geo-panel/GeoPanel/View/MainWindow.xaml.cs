﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Threading;
using System.Diagnostics;

namespace GeoPanel
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : MetroWindow
	{
		/// <summary>
		/// Язык.
		/// </summary>
		private bool language;
		/// <summary>
		/// Инициализация окна
		/// </summary>
		public MainWindow()
		{
			Thread.CurrentThread.CurrentUICulture= CultureInfo.CreateSpecificCulture("eu-ES");
			language = true;
			DataContext = new Controller();
			InitializeComponent();
		}

		/// <summary>
		/// Обработчик кнопки смены языка.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Button_Click(object sender, RoutedEventArgs e)
		{
			if (language)
			{
				language = false;
				table.Columns[0].Header = "Date and time";
				table.Columns[1].Header = "First point";
				table.Columns[2].Header = "Second point";
				table.Columns[3].Header = "Type of result";
				table.Columns[4].Header = "Result";
			}
			else
			{
				language = true;
				table.Columns[0].Header = "Дата и время";
				table.Columns[1].Header = "Первая точка";
				table.Columns[2].Header = "Вторая точка";
				table.Columns[3].Header = "Тип вычислений";
				table.Columns[4].Header = "Результат";
			}
		}

		/// <summary>
		/// Обработка неправильного ввода.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CalcCheck(object sender, RoutedEventArgs e)
		{
			bool firstError = false;
			bool secondError = false;
			if (!Double.TryParse(Lat1.Text, out  _) || !Double.TryParse(Lon1.Text, out _) || !Double.TryParse(Height1.Text, out _) || !Double.TryParse(GeoFirst.Text, out _) || !Double.TryParse(GeoThird.Text, out _) || !Double.TryParse(GeoSecond.Text, out _))
			{
				firstError = true;
			}
			else if (!Double.TryParse(Shirota.Text, out _) || !Double.TryParse(Dolgota.Text, out _) || !Double.TryParse(Visota.Text, out _) || !Double.TryParse(X2.Text, out _) || !Double.TryParse(Z2.Text, out _) || !Double.TryParse(Y2.Text, out _))
			{
				firstError = true;
			}

			if (!Double.TryParse(Lat1.Text.Replace(".",","), out _) || !Double.TryParse(Lon1.Text.Replace(".", ","), out _) || !Double.TryParse(Height1.Text.Replace(".", ","), out _) || !Double.TryParse(GeoFirst.Text.Replace(".", ","), out _) || !Double.TryParse(GeoThird.Text.Replace(".", ","), out _) || !Double.TryParse(GeoSecond.Text.Replace(".", ","), out _))
			{
				secondError = true;
			}
			else if (!Double.TryParse(Shirota.Text.Replace(".", ","), out _) || !Double.TryParse(Dolgota.Text.Replace(".", ","), out _) || !Double.TryParse(Visota.Text.Replace(".", ","), out _) || !Double.TryParse(X2.Text.Replace(".", ","), out _) || !Double.TryParse(Z2.Text.Replace(".", ","), out _) || !Double.TryParse(Y2.Text.Replace(".", ","), out _))
			{
				secondError = true;
			}
		
			if(secondError&&firstError)
			{

				CalcButton.ToolTip = "Ошибка при вводе данных!";
				MessageBox.Show("Введены некорректные данные!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}
		/// <summary>
		/// Вспомогательная функция дла обработчика события.
		/// </summary>
		/// <param name="textBox"></param>
		private void TextChange(TextBox textBox)
		{
			int selectedPosition;
			if (textBox!= null)
			{
				selectedPosition = textBox.SelectionStart;
				textBox.Text = textBox.Text.Replace(",", ".");
				textBox.SelectionStart = selectedPosition;
			}
		}
		/// <summary>
		/// Обработчик измения текста, проверка на запятую.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void TextChanged(object sender, TextChangedEventArgs e)
		{
			TextChange(Lat1);
			TextChange(Shirota);
			TextChange(Lon1);
			TextChange(Dolgota);
			TextChange(Height1);
			TextChange(Visota);
			TextChange(GeoFirst);
			TextChange(X2);
			TextChange(GeoSecond);
			TextChange(Y2);
			TextChange(GeoThird);
			TextChange(Z2);
		}
	}
}
