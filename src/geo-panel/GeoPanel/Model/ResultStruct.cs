﻿namespace GeoPanel
{
	/// <summary>
	/// Структура для выбора вида расчёта
	/// </summary>
	public struct ResultStruct
	{
	
		/// <summary>
		/// Инициализация структуры.
		/// </summary>
		/// <param name="trans">Первый режим</param>
		/// <param name="transReverse">Первый режим(дополнительный)</param>
		/// <param name="curvate">Второй режим</param>
		/// <param name="straight">Третий режим</param>
		public ResultStruct(bool trans, bool transReverse, bool curvate, bool straight)
		{
			TransformCoord = trans;
			TransformCoordReverse = transReverse;
			DistСurvature = curvate;
			DistStraight = straight;
		}
		/// <summary>
		/// Режим отвечающий за перевод геоцентрических коордиант в геодезические.
		/// </summary>
		public bool TransformCoord { get; set; }
		/// <summary>
		/// Режим отвечающий за перевод геодезические коордиант в геоцентрические.
		/// </summary>
		public bool TransformCoordReverse { get; set; }
		/// <summary>
		/// Режим отвечающий за расчёт расстояния  между двумя объектами на поверхности Земли с учётом кривизны поверхности Земли
		/// </summary>
		public bool DistСurvature { get; set; }
		/// <summary>
		/// Режим отвечающий за расчёт расстояния по прямой между двумя объектами на поверхности Земли
		/// </summary>
		public bool DistStraight { get; set; }

	}
}
