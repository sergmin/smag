﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using GeoStars;

namespace GeoPanel
{

	/// <summary>
	/// Модель точки
	/// </summary>
	public class GeoModel : INotifyPropertyChanged
	{
		private bool language;
		/// <summary>
		/// Переменная, отвечающая за язык интерфейса, если true-русский,иначе английский.
		/// </summary>
		public bool Language
		{
			get => language;
			set
			{
				language = value;
				OnPropertyChanged("TitleLat");
				OnPropertyChanged("TitleLon");
				OnPropertyChanged("TitleHeight");
				OnPropertyChanged("TitleNorthLat");
				OnPropertyChanged("TitleSouthLat");
				OnPropertyChanged("TitleWestLon");
				OnPropertyChanged("TitleEastLon");
			}
		}
		private readonly CultureInfo culture;
		/// <summary>
		/// Строковое представление широты.
		/// </summary>
		public string TitleLat => ChangeFunc("Latitude,°:", "Широта,°");
		/// <summary>
		/// Строковое представление северной широты.
		/// </summary>
		public string TitleNorthLat => ChangeFunc("Northern latitude", "Северная широта");
		/// <summary>
		/// Строковое представление южной широты.
		/// </summary>
		public string TitleSouthLat => ChangeFunc("Southern latitude", "Южная широта");
		/// <summary>
		/// Строковое представление южной широты.
		/// </summary>
		public string TitleWestLon => ChangeFunc("Western longitude", "Западная долгота");
		/// <summary>
		/// Строковое представление южной широты.
		/// </summary>
		public string TitleEastLon => ChangeFunc("Eastern longitude", "Восточная долгота");
		/// <summary>
		/// Строковое представление долготы.
		/// </summary>
		public string TitleLon => ChangeFunc("Longitude,°", "Долгота,°:");
		/// <summary>
		/// Строковое представление высоты.
		/// </summary>
		public string TitleHeight => ChangeFunc("Height, m:", "Высота, м:");


		/// <summary>
		/// Западная долгота.
		/// </summary>
		private bool westPolarity;
		/// <summary>
		/// Западная долгота.
		/// </summary>
		public bool WestPolarity
		{
			get => westPolarity;
			set
			{
				ChangePolarityFuncLat(westPolarity, value,false);
				westPolarity = value;
				OnPropertyChanged();
			}
		}
		/// <summary>
		/// Северная широта.
		/// </summary>
		private bool northPolarity;
		/// <summary>
		/// Северная широта.
		/// </summary>
		public bool NorthPolarity
		{
			get => northPolarity;
			set
			{
				ChangePolarityFuncLat(northPolarity, value,true);
				northPolarity = value;
				OnPropertyChanged();

			}
		}
		/// <summary>
		/// Восточная долгота.
		/// </summary>
		private bool eastPolarity;
		/// <summary>
		/// Восточная долгота.
		/// </summary>
		public bool EastPolarity
		{
			get => eastPolarity;
			set
			{
				ChangePolarityFuncLat(eastPolarity, value,false);
				eastPolarity = value;
				OnPropertyChanged();

			}
		}
		/// <summary>
		/// Южная широта.
		/// </summary>
		private bool southPolarity;
		/// <summary>
		/// Южная Широта.
		/// </summary>
		public bool SouthPolarity
		{
			get => southPolarity;
			set
			{
				ChangePolarityFuncLat(southPolarity, value,true);
				southPolarity = value;
				OnPropertyChanged();

			}
		}
		/// <summary>
		/// Cтруктура с геоцентрическими координатами.
		/// </summary>
		private XYZ xyz;
		/// <summary>
		/// Геоцентрическая координата X.
		/// </summary>
		public double GeoFirst
		{
			get => xyz.X;
			set
			{
				xyz.X = value;
				UpdateFunc(false);
			}
		}
		/// <summary>
		/// Геоцентрическая координата Y.
		/// </summary>
		public double GeoSecond
		{
			get => xyz.Y;
			set
			{
				xyz.Y = value;
				UpdateFunc(false);
			}
		}
		/// <summary>
		/// Геоцентрическая координата Z.
		/// </summary>
		public double GeoThird
		{
			get => xyz.Z;
			set
			{
				xyz.Z = value;
				UpdateFunc(false);
			}
		}
		/// <summary>
		/// 
		/// </summary>
		private double longitude;
		/// <summary>
		/// Долгота.
		/// </summary>
		public double Longitude
		{
			get => TransformRadDeg(true, llh.Longitude);
			set
			{
				if (value >= 0)
				{
					eastPolarity = true;
					westPolarity = false;
				}
				else
				{
					eastPolarity = false;
					westPolarity = true;
				}
				if (value > 180) value = 180;
				if (value < -180) value = -180;
				longitude = value;
				llh.Longitude = TransformRadDeg(false, longitude);
				UpdateFunc(true);
				OnPropertyChanged("EastPolarity");
				OnPropertyChanged("WestPolarity");
			}
		}
		/// <summary>
		/// Широта.
		/// </summary>
		private double latitude;
		/// <summary>
		/// Широта.
		/// </summary>
		public double Latitude
		{
			get => TransformRadDeg(true, llh.Latitude);
			set
			{
				if (value >= 0)
				{
					northPolarity = true;
					southPolarity = false;
				}
				else
				{
					northPolarity = false;
					southPolarity = true;
				}
				if (value > 90) value = 90;
				if (value < -90) value = -90;
				latitude = value;
				llh.Latitude = TransformRadDeg(false, latitude);
				UpdateFunc(true);
				OnPropertyChanged("NorthPolarity");
				OnPropertyChanged("SouthPolarity");
			}
		}
		/// <summary>
		/// Высота.
		/// </summary>
		public double Height
		{
			get => llh.Height;
			set
			{
				llh.Height = value;
				UpdateFunc(true);

			}
		}
		/// <summary>
		/// Функция, вызываемая при сменеполярности.
		/// </summary>
		/// <param name="polarity"></param>
		/// <param name="val"></param>
		/// <param name="latLon"></param>
		private void ChangePolarityFuncLat(bool polarity, bool val, bool latLon)
		{
			int Helpint = latLon?(int)Latitude:(int)Longitude;
			if (polarity != val && Helpint != 0)
			{
				if(latLon)
				Latitude = -Latitude;
				else
				{
				Longitude = -Longitude;
				}
			}

			OnPropertyChanged("Latitude");
			OnPropertyChanged("Longitude");
		}

		/// <summary>
		/// Функция меняюшая язык.
		/// </summary>
		public string ChangeFunc(string textRus, string textEng) => Language ? textRus : textEng;
		/// <summary>
		/// Структура содержашаая долготу,широту,высоту.
		/// </summary>
		private LatLonHgt llh;
		/// <summary>
		/// Свойство, дающее доступ к структуре с долготой,широтой,высотой.
		/// </summary>
		public LatLonHgt Llh => llh;
		/// <summary>
		/// Начальная Инициализация параметров.
		/// </summary>
		public GeoModel()
		{
			Language = false;
			culture = CultureInfo.CreateSpecificCulture("eu-ES");
			Longitude = 0;
			Latitude = 0;
			NorthPolarity = true;
			EastPolarity = true;
			SouthPolarity = false;
			WestPolarity = false;
			Height = 100;
			xyz = Geo.LatLonHgt2Xyz(llh);
		}
		/// <summary>
		/// Cобытие, возникающее при смене значения свойства.
		/// </summary>
		public event PropertyChangedEventHandler PropertyChanged;
		/// <summary>
		/// Функция, уведомляющая об изменении координат.
		/// </summary>
		/// <param name="typecoord">
		/// true-изменение геоцентрических координат,false-измение геоцентрических координат.
		/// </param>
		public void UpdateFunc(bool typecoord)
		{
			if (typecoord)
			{
				xyz = Geo.LatLonHgt2Xyz(llh);
				OnPropertyChanged("GeoFirst");
				OnPropertyChanged("GeoSecond");
				OnPropertyChanged("GeoThird");
			}
			else
			{
				llh = Geo.Xyz2LatLonHgt(xyz);
				OnPropertyChanged("Latitude");
				OnPropertyChanged("Longitude");
				OnPropertyChanged("Height");
			}
		}


		/// <summary>
		/// Обработчик изменения свойства.
		/// </summary>
		/// <param name="prop"> Название свойства</param>
		public void OnPropertyChanged([CallerMemberName]string prop = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
		/// <summary>
		/// Представление геоцентрических координат в виде строки.
		/// </summary>
		/// <returns>Строковое представление геоцентрических координат.</returns>
		public override string ToString() => " X: " + Math.Round(GeoFirst, 8).ToString(culture) + " м Y: " + Math.Round(GeoSecond, 8).ToString(culture) + " м Z: " + Math.Round(GeoThird, 8).ToString(culture) + " м";
		/// <summary>
		/// Представление геодезических координат в виде русской строки.
		/// </summary>
		/// <returns>Русское строковое представление геодезических координат.</returns>
		public string ToStringRus() => " Широта: " + Math.Round(Latitude, 8).ToString(culture) + "° Долгота: " + Math.Round(Longitude, 8).ToString(culture) + "° Высота: " + Math.Round(Height, 8).ToString(culture) + " м";
		/// <summary>
		/// Представление геодезических координат в виде английской строки.
		/// </summary>
		/// <returns>Английское строковое представление геодезических координат.</returns>
		public string ToStringEng() => " Latitude: " + Math.Round(Latitude, 8).ToString(culture) + "° Longitude: " + Math.Round(Longitude, 8).ToString(culture) + "° Height: " + Math.Round(Height, 8).ToString(culture) + " m";
		/// <summary>
		/// Перевод из градусов в радианы или наооборот.
		/// </summary>
		/// <param name="transType">
		/// true - перевод из радин в градусы,
		/// false - перевод из градусов в радианы.
		/// </param>
		/// <param name="anglesCurrent">
		/// Элементы для перевода.
		/// </param>
		/// <returns>Возвращает градусы или радианы в зависимости от transType.</returns>
		public static double TransformRadDeg(bool transType, double anglesCurrent) => transType ? anglesCurrent * (180 / Math.PI) : Math.PI / 180 * anglesCurrent;
	}
}
