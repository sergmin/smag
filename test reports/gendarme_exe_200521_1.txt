Тестировщик: Долганов Дмитрий.

Результаты тестирования:

Produced on 21.05.2020 17:22:08 for:
   TOGC

Количество зафиксированных ошибок уровня high и medium - 4 ошибок.

Reported Defects:

------------------------------------------------------------
1. AvoidLackOfCohesionOfMethodsRule

Problem: The methods in this class lack cohesion (a higher score is better). This leads to code which is harder to understand and maintain.
* Severity: High, Confidence: Normal
* Target:   GeoPanel.GeoModel
* Details:  Type cohesiveness : 13%

Solution: You can apply the Extract Class or Extract Subclass refactoring.
More info available at: https://github.com/spouliot/gendarme/wiki/Gendarme.Rules.Maintainability.AvoidLackOfCohesionOfMethodsRule(2.10)


------------------------------------------------------------
2. DoNotHardcodePathsRule

Problem: This string looks like a path that may become invalid if the code is executed on a different operating system.
* Severity: High, Confidence: Normal
* Target:   System.Void GeoPanel.App::InitializeComponent()
* Source:   debugging symbols unavailable, IL offset 0x0027
* Details:  string "/TOGC;component/app.xaml" looks quite like a filename.

Solution: Use System.IO.Path and System.Environment to generate paths instead of hardcoding them.
More info available at: https://github.com/spouliot/gendarme/wiki/Gendarme.Rules.Portability.DoNotHardcodePathsRule(2.10)


3. DoNotHardcodePathsRule

Problem: This string looks like a path that may become invalid if the code is executed on a different operating system.
* Severity: High, Confidence: Normal
* Target:   System.Void GeoPanel.MainWindow::InitializeComponent()
* Source:   debugging symbols unavailable, IL offset 0x0015
* Details:  string "/TOGC;component/view/mainwindow.xaml" looks quite like a filename.

Solution: Use System.IO.Path and System.Environment to generate paths instead of hardcoding them.
More info available at: https://github.com/spouliot/gendarme/wiki/Gendarme.Rules.Portability.DoNotHardcodePathsRule(2.10)


------------------------------------------------------------
4. MainShouldNotBePublicRule

Problem: The entry point (Main) of this assembly is visible to the outside world (ref: C# Programming Guide).
* Severity: Medium, Confidence: Total
* Target:   TOGC, Version=1.1.0.0, Culture=neutral, PublicKeyToken=null
* Location: System.Void GeoPanel.App::Main()
* Details:  Change method visibility to private or internal.

Solution: Reduce the visibility of the method or type if your language allows it. It may not be possible in some language, like VB.NET).
More info available at: https://github.com/spouliot/gendarme/wiki/Gendarme.Rules.Design.MainShouldNotBePublicRule(2.10)


Processed 256 rules.

Комментарий: этап считается успешно пройденным, так как во время тестирования было зафиксировано не более 0.8% ошибок с уровнем "Severity: High" и "Severity: Medium" от общего количества строк кода всех разработанных компонентов - 1045 строк.
