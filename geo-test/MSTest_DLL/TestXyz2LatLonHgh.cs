﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeoStars;

namespace MSTest_DLL
{
    /// <summary>
    /// Тестовый класс для метода Geo.Xyz2LatLonHgt.
    /// </summary>
    /// <seealso cref="Xyz2LatLonHgt(XYZ xyz)"/>
    [TestClass]
    public class TestXyz2LatLonHgh
    {
        /// <summary>
        ///  Допустимая в соответствии с требованиями погрешность для величин, измеряемых в метрах.
        /// </summary>
        private const double DELTA_METERS = 0.1;

        /// <summary>
        ///  Допустимая в соответствии с требованиями погрешность для величин, измеряемых в радианах.
        /// </summary>
        private const double DELTA_RADIANS = 1.0e-6;

        /// <summary>
        /// Объявление используемых в ходе тестирования переменных.
        /// </summary>
        private XYZ xyzTest1, xyzTest2, xyzTest3, xyzTest4, xyzTest0;
        private LatLonHgt latLonHghTest1Web, latLonHghTest2Web, latLonHghTest3Web,
            latLonHghTest4Web, latLonHghTest0Web;

        /// <summary>
        /// Метод формирования наборов тестовых данных
        /// Для формирования тестовых данных в виде LatLonHgt было решено использовать уже протестированный метод Geo.Deg2Rads
        /// </summary>
        [TestInitialize]
        public void TestInitialize()
        {
            //Два набора тестовых данных с "нормальными" значениями: взяты случайные значения координат в допустимых пределах.
            xyzTest1 = new XYZ(2549836.869467189, 3828689.215903673, 4403650.835782587);
            xyzTest2 = new XYZ(1007598.36983327, -371842.54464211, -6266537.78662998);

            // Два набора тестовых данных с "критическими" значениями: взяты максимально возможные и близкие к ним значения координат.
            xyzTest3 = new XYZ(0, 0, 6356752.31415482);
            xyzTest4 = new XYZ(-111671.359166306, 1949.23082487017, 6355787.68038021);

            // Набор тестовых данных с "нулевыми" значениями
            xyzTest0 = new XYZ(6378136.9364, 0.0372, 0.0216);

            // Набор достоверных данных, полученный из набора данных с помошью веб-сервиса http://www.apsalin.com/convert-geodetic-to-cartesian.aspx.
            latLonHghTest1Web = new LatLonHgt(
                Geo.Deg2Rads(new DegMinSec(43, 56, 33.4), Direction.North),
                Geo.Deg2Rads(new DegMinSec(56, 20, 13.6), Direction.East),
                213.77172093);
            latLonHghTest2Web = new LatLonHgt(
                Geo.Deg2Rads(new DegMinSec(80, 20.1, 12.4), Direction.South),
                Geo.Deg2Rads(new DegMinSec(20, 14.4, 57.6), Direction.West),
                550.2378545485);
            latLonHghTest3Web = new LatLonHgt(
                Geo.Deg2Rads(new DegMinSec(90, 0, 0), Direction.North),
                Geo.Deg2Rads(new DegMinSec(180, 0, 0), Direction.East),
                0);
            latLonHghTest4Web = new LatLonHgt(
                Geo.Deg2Rads(new DegMinSec(89, 0, 0), Direction.North),
                Geo.Deg2Rads(new DegMinSec(179, 0, 0), Direction.East),
                10.05536255);
            latLonHghTest0Web = new LatLonHgt(
                Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.North),
                Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.East),
                0);
        }

        /// <summary>
        /// Определение метода тестирования функции Geo.LatLonHgt2Xyz
        /// </summary>
        /// <param name="toFunc">Данные для передачи на вход тестируемой функции.</param>
        /// <param name="expected">Ожидаемые данные на выходе.</param>
        private void RunXyz2LatLonHgt(XYZ toFunc, LatLonHgt expected)
        {
            var llh = Geo.Xyz2LatLonHgt(toFunc);
            Assert.AreEqual(expected.Latitude, llh.Latitude, DELTA_RADIANS);
            Assert.AreEqual(expected.Longitude, llh.Longitude, DELTA_RADIANS);
            Assert.AreEqual(expected.Height, llh.Height, DELTA_METERS);
        }

        /// <summary>
        /// Тест функции Geo.RunXyz2LatLonHgt с набором xyzTest1 -> latLonHghTest1Web
        /// </summary>
        [TestMethod]
        public void Test1()
        {
            RunXyz2LatLonHgt(xyzTest1, latLonHghTest1Web);
        }
        /// <summary>
        /// Тест функции Geo.RunXyz2LatLonHgt с набором xyzTest2 -> latLonHghTest2Web
        /// </summary>
        [TestMethod]
        public void Test2()
        {
            RunXyz2LatLonHgt(xyzTest2, latLonHghTest2Web);
        }
        /// <summary>
        /// Тест функции Geo.RunXyz2LatLonHgt с набором xyzTest3 -> latLonHghTest3Web
        /// </summary>
        [TestMethod]
        public void Test3()
        {
            RunXyz2LatLonHgt(xyzTest3, latLonHghTest3Web);
        }
        /// <summary>
        /// Тест функции Geo.RunXyz2LatLonHgt с набором xyzTest4 -> latLonHghTest4Web
        /// </summary>
        [TestMethod]
        public void Test4()
        {
            RunXyz2LatLonHgt(xyzTest4, latLonHghTest4Web);
        }
        /// <summary>
        /// Тест функции Geo.RunXyz2LatLonHgt с набором xyzTest0 -> latLonHghTest0Web
        /// </summary>
        [TestMethod]
        public void TestZero()
        {
            RunXyz2LatLonHgt(xyzTest0, latLonHghTest0Web);
        }
    }
}
