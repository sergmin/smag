﻿using GeoStars;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSTest_DLL
{
    /// <summary>
    /// Тестовый класс для метода Geo.Deg2Rads.
    /// </summary>
    /// <seealso cref="Deg2Rads(DegMinSec, Direction)"/>
    [TestClass]
    public class TestDeg2Rads
    {
        /// <summary>
        /// Допустимая погрешность в соответствии с требованиями.
        /// </summary> 
        private const double DELTA = 1e-6;

        /// <summary>
        /// Наборы тестовых данных: три "нормальных" значения, два "критических" и два "нулевых".
        /// </summary>
        private readonly DegMinSec N = new DegMinSec(56, 20, 13.6);
        private readonly DegMinSec E = new DegMinSec(43, 56, 33.4);
        private readonly DegMinSec S = new DegMinSec(37, 44, 26.7);
        private readonly DegMinSec P3 = new DegMinSec(750, 0, 0);
        private readonly DegMinSec N3 = new DegMinSec(600, 40, 25);
        private readonly DegMinSec P0 = new DegMinSec(0, 0, 0);
        private readonly DegMinSec N0 = new DegMinSec(0, 0, 0);

        /// <summary>
        /// Набор тестовых данных для одного теста.
        /// </summary> 
        readonly DegMinSec W = new DegMinSec(65, 26, 13.4);
        /// <summary>
        /// Набор достоверных данных, полученный из набора данных с помошью веб-сервиса https://www.rapidtables.com/.
        /// </summary>
        private const double W_RADS_WEB = -1.14209207225489;

        /// <summary>
        /// Наборы достоверных данных, полученных из набора данных с помошью веб-сервиса https://www.rapidtables.com/.
        /// Каждое значение из набора взаимно однозначно соответствует одному из значений из набора тестовых данных.
        /// </summary>
        private const double N_RADS_WEB = 0.98326807995077;
        private const double E_RADS_WEB = 0.76694324581233;
        private const double S_RADS_WEB = -0.65870034967205;
        private const double P3_RADS_WEB = 13.0899693899574718;
        private const double N3_RADS_WEB = -10.483732243732883;
        private const double P0_RADS_WEB = 0.0;
        private const double N0_RADS_WEB = 0.0;

        /// <summary>
        /// Тестовый метод для Geo.Deg2Rads с набором тестовых данных N и Direction.North.
        /// </summary>
        [TestMethod]
        public void TestN()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var nRad = Geo.Deg2Rads(N, Direction.North);
            Assert.AreEqual(N_RADS_WEB, nRad, DELTA);
        }
        /// <summary>
        /// Тестовый метод для Geo.Deg2Rads с набором тестовых данных N и Direction.Positive.
        /// </summary>
        [TestMethod]
        public void TestPositive()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var nRad = Geo.Deg2Rads(N, Direction.Positive);
            Assert.AreEqual(N_RADS_WEB, nRad, DELTA);
        }

        /// <summary>
        /// Тестовый метод для Geo.Deg2Rads с набором тестовых данных E и Direction.East.
        /// </summary>
        [TestMethod]
        public void TestE()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var eRad = Geo.Deg2Rads(E, Direction.East);
            Assert.AreEqual(E_RADS_WEB, eRad, DELTA);
        }
        /// <summary>
        /// Тестовый метод для Geo.Deg2Rads с набором тестовых данных E и Direction.Positive.
        /// </summary>
        [TestMethod]
        public void TestPositive2()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var eRad = Geo.Deg2Rads(E, Direction.Positive);
            Assert.AreEqual(E_RADS_WEB, eRad, DELTA);
        }

        /// <summary>
        /// Тестовый метод для Geo.Deg2Rads с набором тестовых данных S и Direction.South.
        /// </summary>
        [TestMethod]
        public void TestS()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var sRad = Geo.Deg2Rads(S, Direction.South);
            Assert.AreEqual(S_RADS_WEB, sRad, DELTA);
        }

        /// <summary>
        /// Тестовый метод для Geo.Deg2Rads с набором тестовых данных S и Direction.Negative.
        /// </summary>
        [TestMethod]
        public void TestNegative()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var sRad = Geo.Deg2Rads(S, Direction.Negative);
            Assert.AreEqual(S_RADS_WEB, sRad, DELTA);
        }

        /// <summary>
        /// Тестовый метод для Geo.Deg2Rads с набором тестовых данных W и Direction.West.
        /// </summary>
        [TestMethod]
        public void TestW()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var wRad = Geo.Deg2Rads(W, Direction.West);
            Assert.AreEqual(W_RADS_WEB, wRad, DELTA);
        }
        /// <summary>
        /// Тестовый метод для Geo.Deg2Rads с набором тестовых данных W и Direction.Negative.
        /// </summary>
        [TestMethod]
        public void TestNegative2()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var wRad = Geo.Deg2Rads(W, Direction.Negative);
            Assert.AreEqual(W_RADS_WEB, wRad, DELTA);
        }

        /// <summary>
        /// Тестовый метод для Geo.Deg2Rads с набором данных с большими величинами градусов и Direction.Positive.
        /// </summary>
        [TestMethod]
        public void TestPositive3()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var pRad = Geo.Deg2Rads(P3, Direction.Positive);
            Assert.AreEqual(P3_RADS_WEB, pRad, DELTA);
        }

        /// <summary>
        /// Тестовый метод для Geo.Deg2Rads с набором данных с большими величинами градусов и Direction.Negative.
        /// </summary>
        [TestMethod]
        public void TestNegative3()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var nRad = Geo.Deg2Rads(N3, Direction.Negative);
            Assert.AreEqual(N3_RADS_WEB, nRad, DELTA);
        }

        /// <summary>
        /// Тестовый метод для Geo.Deg2Rads с наборами нулевых тестовых данных.
        /// </summary>
        [TestMethod]
        public void TestZero()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var pRad = Geo.Deg2Rads(P0, Direction.Positive);
            Assert.AreEqual(P0_RADS_WEB, pRad, DELTA);

            // Переменная для записи значения, полученного в тестируемом методе
            var nRad = Geo.Deg2Rads(N0, Direction.Negative);
            Assert.AreEqual(N0_RADS_WEB, nRad, DELTA);
        }
    }
}
