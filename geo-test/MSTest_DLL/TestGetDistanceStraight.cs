﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeoStars;

namespace MSTest_DLL
{
    /// <summary>
    /// Тестовый класс для метода Geo.GetDistanceStraight.
    /// </summary>
    /// <seealso cref="GetDistanceStraight(LatLonHgt point1, LatLonHgt point2)"/>
    [TestClass]
    public class TestGetDistanceStraight
    {
        /// <summary>
        ///  Допустимая в соответствии с требованиями погрешность для величин, измеряемых в метрах.
        /// </summary>
        private const double DELTA_STRAIGHT = 2;

        /// <summary>
        /// Два набора тестовых данных с "нормальными" значениями: взяты случайные значения координат в допустимых пределах.
        /// </summary>
        private readonly LatLonHgt FIRST_POINT_1 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(55, 45, 0), Direction.North),
            Geo.Deg2Rads(new DegMinSec(37, 37, 0), Direction.East),
            0);
        private readonly LatLonHgt SECOND_POINT_1 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(59, 53, 0), Direction.North),
            Geo.Deg2Rads(new DegMinSec(30, 15, 0), Direction.East),
            0);

        private readonly LatLonHgt FIRST_POINT_2 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(38, 53, 0), Direction.North),
            Geo.Deg2Rads(new DegMinSec(77, 0, 0), Direction.West),
            122);
        private readonly LatLonHgt SECOND_POINT_2 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(48, 50, 0), Direction.North),
            Geo.Deg2Rads(new DegMinSec(2, 0, 0), Direction.East),
            50);

        /// <summary>
        /// Набор тестовых данных с "критическими" значениями: взяты максимально возможные значения координат.
        /// </summary>
        private readonly LatLonHgt FIRST_POINT_3 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(90, 0, 0), Direction.North),
            Geo.Deg2Rads(new DegMinSec(180, 0, 0), Direction.West),
            0);
        private readonly LatLonHgt SECOND_POINT_3 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(90, 0, 0), Direction.South),
            Geo.Deg2Rads(new DegMinSec(180, 0, 0), Direction.East),
            333);

        /// <summary>
        /// Два набора тестовых данных с "нулевыми" значениями.
        /// В первом все значения координат взяты равными нулю, во втором заданы ненулевые высоты при нулевых широте и долготе.
        /// </summary>
        private readonly LatLonHgt FIRST_POINT_01 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.South),
            Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.West),
            0);
        private readonly LatLonHgt SECOND_POINT_01 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.North),
            Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.East),
            0);

        private readonly LatLonHgt FIRST_POINT_02 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.South),
            Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.West),
            122);
        private readonly LatLonHgt SECOND_POINT_02 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.North),
            Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.East),
            1);

        /// <summary>
        /// Набор достоверных данных, полученных из набора данных с помошью веб-сервисов:
        /// http://www.apsalin.com/convert-geodetic-to-cartesian.aspx - для пересчета географических координат и высоты в прямоугольные
        /// https://www.calculatorsoup.com/calculators/geometry-solids/distance-two-points.php - для нахождения расстояния между полученными точками.
        /// Каждое значение из набора взаимно однозначно соответствует одному из значений из набора тестовых данных.
        /// </summary>
        private const double STRAIGHT_DISTANCE_1_WEB = 634338.681665;
        private const double STRAIGHT_DISTANCE_2_WEB = 5921462.34824;
        private const double STRAIGHT_DISTANCE_3_WEB = 12713837.628491;
        private const double STRAIGHT_DISTANCE_01_WEB = 0;
        private const double STRAIGHT_DISTANCE_02_WEB = 121;

        /// <summary>
        /// Тест метода Geo.GetDistanceCurve с набором тестовых данных FIRST_POINT_1, SECOND_POINT_1 и CURVE_DISTANCE_1_WEB.
        /// </summary>
        [TestMethod]
        public void Test1()
        {
            var distance = Geo.GetDistanceStraight(FIRST_POINT_1, SECOND_POINT_1);
            Assert.AreEqual(STRAIGHT_DISTANCE_1_WEB, distance, DELTA_STRAIGHT);
        }

        /// <summary>
        /// Тест метода Geo.GetDistanceCurve с набором тестовых данных FIRST_POINT_2, SECOND_POINT_2 и CURVE_DISTANCE_2_WEB.
        /// </summary>
        [TestMethod]
        public void Test2()
        {
            var distance = Geo.GetDistanceStraight(FIRST_POINT_2, SECOND_POINT_2);
            Assert.AreEqual(STRAIGHT_DISTANCE_2_WEB, distance, DELTA_STRAIGHT);
        }

        /// <summary>
        /// Тест метода Geo.GetDistanceCurve с набором тестовых данных FIRST_POINT_3, SECOND_POINT_3 и CURVE_DISTANCE_3_WEB.
        /// </summary>
        [TestMethod]
        public void Test3()
        {
            var distance = Geo.GetDistanceStraight(FIRST_POINT_3, SECOND_POINT_3);
            Assert.AreEqual(STRAIGHT_DISTANCE_3_WEB, distance, DELTA_STRAIGHT);
        }

        /// <summary>
        /// Тест метода Geo.GetDistanceCurve с набором тестовых данных FIRST_POINT_01, SECOND_POINT_01 и CURVE_DISTANCE_01_WEB.
        /// </summary>
        [TestMethod]
        public void TestZero1()
        {
            var distance = Geo.GetDistanceStraight(FIRST_POINT_01, SECOND_POINT_01);
            Assert.AreEqual(STRAIGHT_DISTANCE_01_WEB, distance, DELTA_STRAIGHT);
        }

        /// <summary>
        /// Тест метода Geo.GetDistanceCurve с набором тестовых данных FIRST_POINT_02, SECOND_POINT_02 и CURVE_DISTANCE_02_WEB.
        /// </summary>
        [TestMethod]
        public void TestZero2()
        {
            var distance = Geo.GetDistanceStraight(FIRST_POINT_02, SECOND_POINT_02);
            Assert.AreEqual(STRAIGHT_DISTANCE_02_WEB, distance, DELTA_STRAIGHT);
        }
    }
}
