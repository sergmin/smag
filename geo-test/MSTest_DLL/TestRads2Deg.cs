﻿using GeoStars;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MSTest_DLL
{
    /// <summary>
    /// Тестовый класс для метода Geo.Rads2Deg.
    /// </summary>
    /// <seealso cref="Rads2Deg(double radian)"/>
    [TestClass]
    public class TestRads2Deg
    {
        /// <summary>
        /// Допустимая в соответствии с требованиями погрешность.
        /// </summary>
        private const double DELTA = 1e-6;

        /// <summary>
        /// Четыре набора тестовых данных с "нормальными" значениями.
        /// </summary> 
        private const double N = 0.98326807995077;
        private const double E = 0.76694324581233;
        private const double S = -0.65870034967205;
        private const double W = -1.14209207225489;

        /// <summary>
        /// Два набора тестовых данных с "критическими" значениями разных знаков.
        /// </summary>
        private const double POSITIVE = 13.0899693899574718;
        private const double NEGATIVE = -10.483732243732883;

        /// <summary>
        /// Два набора тестовых данных с "нулевыми" значениями.
        /// </summary>
        private const double P0 = 0.0;
        private const double N0 = 0.0;


        /// <summary>
        /// Наборы достоверных данных, полученных из набора данных с помошью веб-сервиса https://www.rapidtables.com/.
        /// Каждое значение из набора взаимно однозначно соответствует одному из значений из набора тестовых данных.
        /// </summary>
        private readonly DegMinSec N_DEG_WEB = new DegMinSec(56, 20, 13.6);
        private readonly DegMinSec E_DEG_WEB = new DegMinSec(43, 56, 33.4);
        private readonly DegMinSec S_DEG_WEB = new DegMinSec(37, 44, 26.7);
        private readonly DegMinSec W_DEG_WEB = new DegMinSec(65, 26, 13.4);
        private readonly DegMinSec POSITIVE_DEG_WEB = new DegMinSec(750, 0, 0);
        private readonly DegMinSec NEGATIVE_DEG_WEB = new DegMinSec(600, 40, 25);
        private readonly DegMinSec P0_DEG_WEB = new DegMinSec(0, 0, 0);
        private readonly DegMinSec N0_DEG_WEB = new DegMinSec(0, 0, 0);

        /// <summary>
        /// Тестовый метод для Geo.Rads2Deg с набором тестовых данных N и N_DEG_WEB.
        /// </summary>
        [TestMethod]
        public void TestN()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var nDeg = Geo.Rads2Deg(N);
            Assert.AreEqual(N_DEG_WEB.Degrees, nDeg.Degrees, DELTA);
            Assert.AreEqual(N_DEG_WEB.Minutes, nDeg.Minutes, DELTA);
            Assert.AreEqual(N_DEG_WEB.Seconds, nDeg.Seconds, DELTA);
        }
                
        /// <summary>
        /// Тестовый метод для Geo.Rads2Deg с набором тестовых данных E и E_DEG_WEB.
        /// </summary>
        [TestMethod]
        public void TestE()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var eDeg = Geo.Rads2Deg(E);
            Assert.AreEqual(E_DEG_WEB.Degrees, eDeg.Degrees, DELTA);
            Assert.AreEqual(E_DEG_WEB.Minutes, eDeg.Minutes, DELTA);
            Assert.AreEqual(E_DEG_WEB.Seconds, eDeg.Seconds, DELTA);
        }
        
        /// <summary>
        /// Тестовый метод для Geo.Rads2Deg с набором тестовых данных S и S_DEG_WEB.
        /// </summary>
        [TestMethod]
        public void TestS()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var sDeg = Geo.Rads2Deg(S);
            Assert.AreEqual(S_DEG_WEB.Degrees, sDeg.Degrees, DELTA);
            Assert.AreEqual(S_DEG_WEB.Minutes, sDeg.Minutes, DELTA);
            Assert.AreEqual(S_DEG_WEB.Seconds, sDeg.Seconds, DELTA);
        }
        
        /// <summary>
        /// Тестовый метод для Geo.Rads2Deg с набором тестовых данных S и S_DEG_WEB.
        /// </summary>
        [TestMethod]
        public void TestW()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var wDeg = Geo.Rads2Deg(W);
            Assert.AreEqual(W_DEG_WEB.Degrees, wDeg.Degrees, DELTA);
            Assert.AreEqual(W_DEG_WEB.Minutes, wDeg.Minutes, DELTA);
            Assert.AreEqual(W_DEG_WEB.Seconds, wDeg.Seconds, DELTA);
        }

        /// <summary>
        /// Тестовый метод для Geo.Rads2Deg с набором тестовых данных POSITIVE и POSITIVE_DEG_WEB.
        /// </summary>
        [TestMethod]
        public void TestPositive()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var posDeg = Geo.Rads2Deg(POSITIVE);
            Assert.AreEqual(POSITIVE_DEG_WEB.Degrees, posDeg.Degrees, DELTA);
            Assert.AreEqual(POSITIVE_DEG_WEB.Minutes, posDeg.Minutes, DELTA);
            Assert.AreEqual(POSITIVE_DEG_WEB.Seconds, posDeg.Seconds, DELTA);
        }
       
        /// <summary>
        /// Тестовый метод для Geo.Rads2Deg с набором тестовых данных NEGATIVE и NEGATIVE_DEG_WEB.
        /// </summary>
        [TestMethod]
        public void TestNegative()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var negDeg = Geo.Rads2Deg(NEGATIVE);
            Assert.AreEqual(NEGATIVE_DEG_WEB.Degrees, negDeg.Degrees, DELTA);
            Assert.AreEqual(NEGATIVE_DEG_WEB.Minutes, negDeg.Minutes, DELTA);
            Assert.AreEqual(NEGATIVE_DEG_WEB.Seconds, negDeg.Seconds, DELTA);
        }

        /// <summary>
        /// Тестовый метод для Geo.Rads2Deg с наборами нулевых тестовых данных P0 -> P0_DEG_WEB и N0 -> N0_DEG_WEB.
        /// </summary>
        [TestMethod]
        public void TestZero()
        {
            // Переменные для записи значениий, полученных в тестируемом методе
            var pDeg = Geo.Rads2Deg(P0);
            var nDeg = Geo.Rads2Deg(N0);

            Assert.AreEqual(P0_DEG_WEB.Degrees, pDeg.Degrees, DELTA);
            Assert.AreEqual(P0_DEG_WEB.Minutes, pDeg.Minutes, DELTA);
            Assert.AreEqual(P0_DEG_WEB.Seconds, pDeg.Seconds, DELTA);
            Assert.AreEqual(N0_DEG_WEB.Degrees, nDeg.Degrees, DELTA);
            Assert.AreEqual(N0_DEG_WEB.Minutes, nDeg.Minutes, DELTA);
            Assert.AreEqual(N0_DEG_WEB.Seconds, nDeg.Seconds, DELTA);
        }
    }
}
