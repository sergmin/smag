﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeoStars;

namespace MSTest_DLL
{
    /// <summary>
    /// Тестовый класс для метода Geo.LatLonHgt2Xyz.
    /// </summary>
    /// <seealso cref="LatLonHgt2Xyz(LatLonHgt latlon)"/>
    [TestClass]
    public class TestLatLonHgh2Xyz
    {
        /// <summary>
        /// Допустимая в соответствии с требованиями погрешность.
        /// </summary>
        private const double DELTA_METERS = 0.1;

        /// <summary>
        /// Объявление используемых в ходе тестирования переменных.
        /// </summary>
        private LatLonHgt latLonHghTest1, latLonHghTest2, latLonHghTest3, latLonHghTest4, latLonHghTest0;
        private XYZ xyzTest1Web, xyzTest2Web, xyzTest3Web, xyzTest4Web, xyzTest0Web;

        /// <summary>
        /// Метод формирования наборов тестовых данных.
        /// Прим.: Для формирования тестовых данных в виде LatLonHgt было решено использовать уже протестированный метод Geo.Deg2Rads.
        /// </summary>
        [TestInitialize]
        public void TestInitialize()
        {
            //Два набора тестовых данных с "нормальными" значениями: взяты случайные значения координат в допустимых пределах.
            latLonHghTest1 = new LatLonHgt(
                Geo.Deg2Rads(new DegMinSec(43, 56, 33.4), Direction.North),
                Geo.Deg2Rads(new DegMinSec(56, 20, 13.6), Direction.East),
                213.77172093);

            latLonHghTest2 = new LatLonHgt(
                Geo.Deg2Rads(new DegMinSec(80, 20.1, 12.4), Direction.South),
                Geo.Deg2Rads(new DegMinSec(20, 14.4, 57.6), Direction.West),
                550.2378545485);

            // Два набора тестовых данных с "критическими" значениями: взяты максимально возможные и близкие к ним значения координат.
            latLonHghTest3 = new LatLonHgt(
                Geo.Deg2Rads(new DegMinSec(90, 0, 0), Direction.North),
                Geo.Deg2Rads(new DegMinSec(180, 0, 0), Direction.East),
                0);

            latLonHghTest4 = new LatLonHgt(
                Geo.Deg2Rads(new DegMinSec(89, 0, 0), Direction.North),
                Geo.Deg2Rads(new DegMinSec(179, 0, 0), Direction.East),
                10.05536255);

            // Набор тестовых данных с "нулевыми" значениями: все значения координат взяты равными нулю.
            latLonHghTest0 = new LatLonHgt(
                Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.North),
                Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.East),
                0);

            // Набор достоверных данных, полученный из набора данных с помошью веб-сервиса http://www.apsalin.com/convert-geodetic-to-cartesian.aspx.
            // Каждое значение из набора взаимно однозначно соответствует одному из значений из набора тестовых данных.
            xyzTest1Web = new XYZ( 2549836.869467189, 3828689.215903673, 4403650.835782587);
            xyzTest2Web = new XYZ( 1007598.36983327, -371842.54464211, -6266537.78662998);
            xyzTest3Web = new XYZ( 0, 0, 6356752.31415482);
            xyzTest4Web = new XYZ( -111671.359166306, 1949.23082487017, 6355787.68038021);
            xyzTest0Web = new XYZ( 6378136.9364, 0.0372, 0.0216);
        }

        /// <summary>
        /// Определение метода тестирования функции Geo.LatLonHgt2Xyz
        /// </summary>
        /// <param name="toFunc">Данные для передачи на вход тестируемой функции.</param>
        /// <param name="expected">Ожидаемые данные на выходе.</param>
        private void RunLatLonHgh2Xyz(LatLonHgt toFunc, XYZ expected)
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var xyz = Geo.LatLonHgt2Xyz(toFunc);
            Assert.AreEqual(expected.X, xyz.X, DELTA_METERS);
            Assert.AreEqual(expected.Y, xyz.Y, DELTA_METERS);
            Assert.AreEqual(expected.Z, xyz.Z, DELTA_METERS);
        }

        /// <summary>
        /// Тест функции Geo.LatLonHgt2Xyz с набором latLonHghTest1 -> xyzTest1Web
        /// </summary>
        [TestMethod]
        public void Test1()
        {
            RunLatLonHgh2Xyz(latLonHghTest1, xyzTest1Web);
        }
        /// <summary>
        /// Тест функции Geo.LatLonHgt2Xyz с набором latLonHghTest2 -> xyzTest2Web
        /// </summary>
        [TestMethod]
        public void Test2()
        {
            RunLatLonHgh2Xyz(latLonHghTest2, xyzTest2Web);
        }
        /// <summary>
        /// Тест функции Geo.LatLonHgt2Xyz с набором latLonHghTest3 -> xyzTest3Web
        /// </summary>
        [TestMethod]
        public void Test3()
        {
            RunLatLonHgh2Xyz(latLonHghTest3, xyzTest3Web);
        }
        /// <summary>
        /// Тест функции Geo.LatLonHgt2Xyz с набором latLonHghTest4 -> xyzTest4Web
        /// </summary>
        [TestMethod]
        public void Test4()
        {
            RunLatLonHgh2Xyz(latLonHghTest4, xyzTest4Web);
        }
        /// <summary>
        /// Тест функции Geo.LatLonHgt2Xyz с набором latLonHghTest0 -> xyzTest0Web
        /// </summary>
        [TestMethod]
        public void TestZero()
        {
            RunLatLonHgh2Xyz(latLonHghTest0, xyzTest0Web);
        }
    }
}
