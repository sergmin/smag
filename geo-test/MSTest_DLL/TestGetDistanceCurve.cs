﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeoStars;

namespace MSTest_DLL
{
    /// <summary>
    /// Тестовый класс для метода Geo.GetDistanceCurve.
    /// </summary>
    /// <seealso cref="GetDistanceCurve(LatLonHgt point1, LatLonHgt point2)"/>
    [TestClass]
    public class TestGetDistanceCurve
    {
        /// <summary>
        /// Допустимая погрешность в соответствии с требованиями.
        /// </summary>
        private const double DELTA_CURVE = 5;

        /// <summary>
        /// Два набора тестовых данных с "нормальными" значениями: взяты случайные значения координат в допустимых пределах.
        /// </summary>
        private readonly LatLonHgt FIRST_POINT_1 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(55, 45, 0), Direction.North),
            Geo.Deg2Rads(new DegMinSec(37, 37, 0), Direction.East),
            23);
        private readonly LatLonHgt SECOND_POINT_1 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(59, 53, 0), Direction.North),
            Geo.Deg2Rads(new DegMinSec(30, 15, 0), Direction.East),
            123);

        private readonly LatLonHgt FIRST_POINT_2 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(38, 53, 0), Direction.North),
            Geo.Deg2Rads(new DegMinSec(77, 0, 0), Direction.West),
            122);
        private readonly LatLonHgt SECOND_POINT_2 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(48, 50, 0), Direction.North),
            Geo.Deg2Rads(new DegMinSec(2, 0, 0), Direction.East),
            50);

        /// <summary>
        /// Набор тестовых данных с "критическими" значениями: взяты максимально возможные значения координат.
        /// </summary>
        private readonly LatLonHgt FIRST_POINT_3 = new LatLonHgt(
           Geo.Deg2Rads(new DegMinSec(90, 0, 0), Direction.North),
           Geo.Deg2Rads(new DegMinSec(180, 0, 0), Direction.West),
           0);
        private readonly LatLonHgt SECOND_POINT_3 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(90, 0, 0), Direction.South),
            Geo.Deg2Rads(new DegMinSec(180, 0, 0), Direction.East),
            333);

        /// <summary>
        /// Набор тестовых данных с "нулевыми" значениями: все значения координат взяты равными нулю.
        /// </summary>
        private readonly LatLonHgt FIRST_POINT_0 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.South),
            Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.West),
            12);
        private readonly LatLonHgt SECOND_POINT_0 = new LatLonHgt(
            Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.North),
            Geo.Deg2Rads(new DegMinSec(0, 0, 0), Direction.East),
            1);

        /// <summary>
        /// Набор достоверных данных, полученных из набора данных с помошью веб-сервиса https://planetcalc.ru/.
        /// Каждое значение из набора взаимно однозначно соответствует одному из значений из набора тестовых данных.
        /// </summary>
        private const double CURVE_DISTANCE_1_WEB = 633006.7573;
        private const double CURVE_DISTANCE_2_WEB = 6140948.3611154;
        private const double CURVE_DISTANCE_3_WEB = 20015114.3521864;
        private const double CURVE_DISTANCE_0_WEB = 0;

        /// <summary>
        /// Тест метода Geo.GetDistanceCurve с набором тестовых данных FIRST_POINT_1, SECOND_POINT_1 и CURVE_DISTANCE_1_WEB.
        /// </summary>
        [TestMethod]
        public void Test1()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var distance = Geo.GetDistanceCurve(FIRST_POINT_1, SECOND_POINT_1);
            Assert.AreEqual(CURVE_DISTANCE_1_WEB, distance, DELTA_CURVE);
        }

        /// <summary>
        /// Тест метода Geo.GetDistanceCurve с набором тестовых данных FIRST_POINT_2, SECOND_POINT_2 и CURVE_DISTANCE_2_WEB.
        /// </summary>
        [TestMethod]
        public void Test2()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var distance = Geo.GetDistanceCurve(FIRST_POINT_2, SECOND_POINT_2);
            Assert.AreEqual(CURVE_DISTANCE_2_WEB, distance, DELTA_CURVE);
        }

        /// <summary>
        /// Тест метода Geo.GetDistanceCurve с набором тестовых данных FIRST_POINT_3, SECOND_POINT_3 и CURVE_DISTANCE_3_WEB.
        /// </summary>
        [TestMethod]
        public void Test3()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var distance = Geo.GetDistanceCurve(FIRST_POINT_3, SECOND_POINT_3);
            Assert.AreEqual(CURVE_DISTANCE_3_WEB, distance, DELTA_CURVE);
        }

        /// <summary>
        /// Тест метода Geo.GetDistanceCurve с набором тестовых данных FIRST_POINT_0, SECOND_POINT_0 и CURVE_DISTANCE_0_WEB.
        /// </summary>
        [TestMethod]
        public void TestZero()
        {
            // Переменная для записи значения, полученного в тестируемом методе
            var distance = Geo.GetDistanceCurve(FIRST_POINT_0, SECOND_POINT_0);
            Assert.AreEqual(CURVE_DISTANCE_0_WEB, distance, DELTA_CURVE);
        }
    }
}
