﻿namespace CodedUI_Panel
{
    using System;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using System.Globalization;
    using System.Text.RegularExpressions;


    /// <summary>
    /// Взаимодействия с элементами интерфейса пользователя.
    /// </summary>
    public partial class UIMap
    {
        /// <summary>
        /// Координаты точки.
        /// </summary>
        public struct Coordinate
        {
            /// <summary>
            /// Координата X.
            /// </summary>
            public double x { get; }
            /// <summary>
            /// Координата Y.
            /// </summary>
            public double y { get; }
            /// <summary>
            /// Координата Z.
            /// </summary>
            public double z { get; }

            /// <summary>
            /// Широта.
            /// </summary>
            public double lat { get; }
            /// <summary>
            /// Долгота.
            /// </summary>
            public double lng { get; }
            /// <summary>
            /// Высота.
            /// </summary>
            public double height { get; }

            /// <summary>
            /// Если true, то северная широта, если false - южная.
            /// </summary>
            public bool northLat { get; }
            /// <summary>
            /// Если true, то восточная долгота, если false - западная.
            /// </summary>
            public bool eastLong { get; }

            /// <summary>
            /// Создание точки. Координата XYZ должна соответствовать LatLngHeight.
            /// </summary>
            /// <param name="x">Координата X.</param>
            /// <param name="y">Координата Y.</param>
            /// <param name="z">Координата X.</param>
            /// <param name="lat">Широта.</param>
            /// <param name="lng">Долгота.</param>
            /// <param name="height">Высота.</param>
            /// <param name="northLat">Если true, то северная широта, если false - южная.</param>
            /// <param name="eastLong">Если true, то восточная долгота, если false - западная.</param>
            public Coordinate(double x, double y, double z, double lat, double lng, double height, bool northLat, bool eastLong)
            {
                this.x = x;
                this.y = y;
                this.z = z;

                this.lat = lat;
                this.lng = lng;
                this.height = height;

                this.northLat = northLat;
                this.eastLong = eastLong;
            }
        }

        /// <summary>
        /// Получение значения ячейки в столбце результат.
        /// </summary>
        /// <param name="value">Не преобразованное значение ячейки.</param>
        /// <param name="x">Координата X или Lat.</param>
        /// <param name="y">Координата Y или Lng.</param>
        /// <param name="z">Координата Z или Height.</param>
        private void GetCellTransfValue(string value, out double x, out double y, out double z)
        {
            // Убрать все символы кроме цифр, точки, запятой, тире.
            Regex regex = new Regex(@"[^- \d . , E]");
            value = regex.Replace(value, "");
            regex = new Regex(@"\s+");
            value = regex.Replace(value, "|");
            value = value.Substring(1, value.Length - 2);
            System.Diagnostics.Debug.WriteLine(value);

            var texts = value.Split('|');
            var xyz = new double[3];
            if (texts.Length != xyz.Length)
                throw new Exception();

            for (var i = 0; i < texts.Length; i++)
                xyz[i] = double.Parse(texts[i], CultureInfo.CurrentCulture);

            x = xyz[0]; y = xyz[1]; z = xyz[2];
        }

        /// <summary>
        /// Получение значения ячейки "результат".
        /// </summary>
        /// <returns>Значение ячейки</returns>
        public double GetCellResult()
        {
            string txt = UITransformationofGeodWindow.UITableTable.GetCell(0, 4).Value;
            if (txt != null && txt != string.Empty)
            {
                txt = txt.Substring(0, txt.Length - 2);
                return double.Parse(txt, CultureInfo.CurrentCulture);
            }
            else return double.NaN;
        }

        /// <summary>
        /// Запуск приложения.
        /// </summary>
        public void LaunchApp()
        {
            const string path = "..\\..\\..\\..\\..\\src\\geo-panel\\GeoPanel\\bin\\Debug\\TOGC.exe";
            ApplicationUnderTest.Launch(path, path);
        }

        /// <summary>
        /// Запись координаты XYZ первой точки.
        /// </summary>
        /// <param name="x">X.</param>
        /// <param name="y">Y.</param>
        /// <param name="z">Z.</param>
        public void FirstPointSetXyz(string x, string y, string z)
        {
            UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIКоординатыпервойточкGroup.UIGeoFirstEdit.Text = x;
            UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIКоординатыпервойточкGroup.UIGeoSecondEdit.Text = y;
            UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIКоординатыпервойточкGroup.UIGeoThirdEdit.Text = z;
        }

        /// <summary>
        /// Получение координаты XYZ первой точки.
        /// </summary>
        /// <param name="x">X.</param>
        /// <param name="y">Y.</param>
        /// <param name="z">Z.</param>
        public void FirstPointGetXyz(out double x, out double y, out double z)
        {
            x = double.Parse(UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIКоординатыпервойточкGroup.UIGeoFirstEdit.Text, CultureInfo.InvariantCulture);
            y = double.Parse(UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIКоординатыпервойточкGroup.UIGeoSecondEdit.Text, CultureInfo.InvariantCulture);
            z = double.Parse(UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIКоординатыпервойточкGroup.UIGeoThirdEdit.Text, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Получение координаты XYZ первой точки в таблице.
        /// </summary>
        /// <param name="x">X.</param>
        /// <param name="y">Y.</param>
        /// <param name="z">Z.</param>
        public void FirstPointGetXyzTable(out double x, out double y, out double z)
        {
            if (UITransformationofGeodWindow.UITableTable.RowCount > 1)
            {
                string txt = UITransformationofGeodWindow.UITableTable.GetCell(0, 1).Value;
                GetCellTransfValue(txt, out x, out y, out z);
            }
            else
            {
                x = double.NaN;
                y = double.NaN;
                z = double.NaN;
            }
        }

        /// <summary>
        /// Запись координаты LatLngHeight первой точки.
        /// </summary>
        /// <param name="lat">Широта.</param>
        /// <param name="lng">Долгота.</param>
        /// <param name="height">Высота.</param>
        public void FirstPointSetLlh(string lat, string lng, string height)
        {
            UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIКоординатыпервойточкGroup.UILat1Edit.Text = lat;
            UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIКоординатыпервойточкGroup.UILon1Edit.Text = lng;
            UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIКоординатыпервойточкGroup.UIHeight1Edit.Text = height;
        }

        /// <summary>
        /// Получение координаты LatLngHeight первой точки
        /// </summary>
        /// <param name="lat">Широта.</param>
        /// <param name="lng">Долгота.</param>
        /// <param name="height">Высота.</param>
        public void FirstPointGetLlh(out double lat, out double lng, out double height)
        {
            lat = double.Parse(UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIКоординатыпервойточкGroup.UILat1Edit.Text, CultureInfo.InvariantCulture);
            lng = double.Parse(UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIКоординатыпервойточкGroup.UILon1Edit.Text, CultureInfo.InvariantCulture);
            height = double.Parse(UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIКоординатыпервойточкGroup.UIHeight1Edit.Text, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Получение координаты LatLngHeight первой точки в таблице
        /// </summary>
        /// <param name="lat">Широта.</param>
        /// <param name="lng">Долгота.</param>
        /// <param name="height">Высота.</param>
        public void FirstPointGetLlhTable(out double lat, out double lng, out double height)
        {
            if (UITransformationofGeodWindow.UITableTable.RowCount > 1)
            {
                string txt = UITransformationofGeodWindow.UITableTable.GetCell(0, 4).Value;
                GetCellTransfValue(txt, out lat, out lng, out height);
            }
            else
            {
                lat = double.NaN;
                lng = double.NaN;
                height = double.NaN;
            }
        }

        /// <summary>
        /// Выбор полярности для первой точки.
        /// </summary>
        /// <param name="northLat">Если true, то северная широта, если false - южная.</param>
        /// <param name="eastLong">Если true, то восточная долгота, если false - западная.</param>
        public void FirstPointSelPolarity(bool northLat, bool eastLong)
        {
            if (northLat)
                UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIПолярностьдолготыишиGroup.UIСевернаяширотаRadioButton.Selected = true;
            else UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIПолярностьдолготыишиGroup.UIЮжнаяширотаRadioButton.Selected = true;

            if (eastLong)
                UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIПолярностьдолготыишиGroup.UIВосточнаядолготаRadioButton.Selected = true;
            else UITransformationofGeodWindow.UIItemTabList.UIПерваяточкаTabPage.UIПолярностьдолготыишиGroup.UIЗападнаядолготаRadioButton.Selected = true;
        }

        /// <summary>
        /// Запись координаты XYZ второй точки.
        /// </summary>
        /// <param name="x">X.</param>
        /// <param name="y">Y.</param>
        /// <param name="z">Z.</param>
        public void SecondPointSetXyz(string x, string y, string z)
        {
            UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIКоординатывторойточкGroup.UIX2Edit.Text = x;
            UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIКоординатывторойточкGroup.UIY2Edit.Text = y;
            UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIКоординатывторойточкGroup.UIZ2Edit.Text = z;
        }

        /// <summary>
        /// Получение координаты XYZ второй точки.
        /// </summary>
        /// <param name="x">X.</param>
        /// <param name="y">Y.</param>
        /// <param name="z">Z.</param>
        public void SecondPointGetXyz(out double x, out double y, out double z)
        {
            x = double.Parse(UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIКоординатывторойточкGroup.UIX2Edit.Text, CultureInfo.InvariantCulture);
            y = double.Parse(UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIКоординатывторойточкGroup.UIY2Edit.Text, CultureInfo.InvariantCulture);
            z = double.Parse(UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIКоординатывторойточкGroup.UIZ2Edit.Text, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Получение координаты XYZ второй точки в таблице.
        /// </summary>
        /// <param name="x">X.</param>
        /// <param name="y">Y.</param>
        /// <param name="z">Z.</param>
        public void SecondPointGetXyzTable(out double x, out double y, out double z)
        {
            if (UITransformationofGeodWindow.UITableTable.RowCount > 1)
            {
                var idxRow = UITransformationofGeodWindow.UITableTable.RowCount - 2;
                string txt = UITransformationofGeodWindow.UITableTable.GetCell(idxRow, 2).Value;
                GetCellTransfValue(txt, out x, out y, out z);
            }
            else
            {
                x = double.NaN;
                y = double.NaN;
                z = double.NaN;
            }
        }

        /// <summary>
        /// Запись координаты LatLngHeight второй точки.
        /// </summary>
        /// <param name="lat">Широта.</param>
        /// <param name="lng">Долгота.</param>
        /// <param name="height">Высота.</param>
        public void SecondPointSetLlh(string lat, string lng, string height)
        {
            UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIКоординатывторойточкGroup.UIShirotaEdit.Text = lat;
            UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIКоординатывторойточкGroup.UIDolgotaEdit.Text = lng;
            UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIКоординатывторойточкGroup.UIVisotaEdit.Text = height;
        }

        /// <summary>
        /// Получение координаты LatLngHeight второй точки
        /// </summary>
        /// <param name="lat">Широта.</param>
        /// <param name="lng">Долгота.</param>
        /// <param name="height">Высота.</param>
        public void SecondPointGetLlh(out double lat, out double lng, out double height)
        {
            lat = double.Parse(UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIКоординатывторойточкGroup.UIShirotaEdit.Text, CultureInfo.InvariantCulture);
            lng = double.Parse(UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIКоординатывторойточкGroup.UIDolgotaEdit.Text, CultureInfo.InvariantCulture);
            height = double.Parse(UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIКоординатывторойточкGroup.UIVisotaEdit.Text, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Получение координаты LatLngHeight второй точки в таблице
        /// </summary>
        /// <param name="lat">Широта.</param>
        /// <param name="lng">Долгота.</param>
        /// <param name="height">Высота.</param>
        public void SecondPointGetLlhTable(out double lat, out double lng, out double height)
        {
            if (UITransformationofGeodWindow.UITableTable.RowCount > 1)
            {
                var idxRow = UITransformationofGeodWindow.UITableTable.RowCount - 2;
                string txt = UITransformationofGeodWindow.UITableTable.GetCell(idxRow, 4).Value;
                GetCellTransfValue(txt, out lat, out lng, out height);
            }
            else
            {
                lat = double.NaN;
                lng = double.NaN;
                height = double.NaN;
            }
        }

        /// <summary>
        /// Выбор полярности для второй точки.
        /// </summary>
        /// <param name="northLat">true – северная широта, false - южная широта.</param>
        /// <param name="eastLong">true – восточная долгота, false - западная долгота.</param>
        public void SecondPointSelPolarity(bool northLat, bool eastLong)
        {
            if (northLat)
                UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIПолярностьдолготыишиGroup.UIСевернаяширотаRadioButton.Selected = true;
            else UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIПолярностьдолготыишиGroup.UIЮжнаяширотаRadioButton.Selected = true;

            if (eastLong)
                UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIПолярностьдолготыишиGroup.UIВосточнаядолготаRadioButton.Selected = true;
            else UITransformationofGeodWindow.UIItemTabList.UIВтораяточкаTabPage.UIПолярностьдолготыишиGroup.UIЗападнаядолготаRadioButton.Selected = true;
        }

        /// <summary>
        /// Выбор расчёта.
        /// </summary>
        /// <param name="act">0 - преобразование в геодезические; 1 - расстояние с учётом кривизны;
        /// 2 - расстояние по прямой; 3 - преобразование в геоцентрические</param>
        public void SelAction(int act)
        {
            switch (act)
            {
                case 0: UITransformationofGeodWindow.UIВидрасчётаGroup.UIПреобразованиегеоценRadioButton.Selected = true; break;
                case 1: UITransformationofGeodWindow.UIВидрасчётаGroup.UIРасчётрасстояниямеждRadioButton.Selected = true; break;
                case 2: UITransformationofGeodWindow.UIВидрасчётаGroup.UIРасчётрасстоянияпопрRadioButton.Selected = true; break;
                case 3: UITransformationofGeodWindow.UIВидрасчётаGroup.UIПреобразованиегеодезRadioButton.Selected = true; break;
                default: break;
            }
        }

    }
}
