﻿using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using System.Diagnostics;
using System.Globalization;


namespace CodedUI_Panel
{
    /// <summary>
    /// Класс для тестирования расчётов расстояния.
    /// </summary>
    [CodedUITest]
    public class DistanceCalc
    {
        /// <summary>
        /// Тестируемые точки.
        /// </summary>
        private readonly UIMap.Coordinate coord1, coord2;

        /// <summary>
        /// Ожидаемое расстояние по прямой.
        /// </summary>
        private readonly double expStreight;

        /// <summary>
        /// Ожидаемое расстояние по дуге.
        /// </summary>
        private readonly double expCurve;

        /// <summary>
        /// Если true - запись координат через LatLngHeight, если false - через XYZ.
        /// </summary>
        private readonly bool radian;

        /// <summary>
        /// Формирование чисел с плавающей точкой при записи.
        /// </summary>
        private readonly CultureInfo cultureInfo = CultureInfo.InvariantCulture;

        /// <summary>
        /// Дельта для расчёта расстояния по прямой (м).
        /// </summary>
        private const double DELTA_STREIGHT = 2;

        /// <summary>
        /// Дельта для расчёта расстояния по дуге (м).
        /// </summary>
        private const double DELTA_CURVE = 5;

        /// <summary>
        /// Получает или устанавливает контекст теста, в котором предоставляются
        /// сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        /// <summary>
        /// Получение объекта класса взаимодействия с элементами интерфейса пользователя.
        /// </summary>
        public UIMap uiMap => map ?? (map = new UIMap());
        private UIMap map;

        /// <summary>
        /// Тип делегата для выбора записи координат: через XYZ или LatLngHeight.
        /// </summary>
        /// <param name="numAction">Выбор действия: 1 - с учётом кривизны, 2 - по прямой.</param>
        private delegate void RadianRecorded(int numAction);

        /// <summary>
        /// Тип делегата для выбора записи координат в виде строки: через XYZ или LatLngHeight.
        /// </summary>
        /// <param name="numAction">Выбор действия: 1 - с учётом кривизны, 2 - по прямой.</param>
        /// <param name="x1">Широта или X первой точки.</param>
        /// <param name="y1">Долгота или Y первой точки.</param>
        /// <param name="z1">Высота или Z первой точки.</param>
        /// <param name="x2">Широта или X второй точки.</param>
        /// <param name="y2">Долгота или Y второй точки.</param>
        /// <param name="z2">Высота или Z второй точки.</param>
        private delegate void RadianStringRecorded(int numAction, string x1, string y1, string z1, string x2, string y2, string z2);

        /// <summary>
        /// Делегат для записи координаты точек.
        /// </summary>
        private RadianRecorded DistanceRecord;

        /// <summary>
        /// Делегат для записи координаты точек в виде строки.
        /// </summary>
        private RadianStringRecorded DistanceStringRecord;

        /// <summary>
        /// Инициализация полей класса для тестирования.
        /// </summary>
        public DistanceCalc()
        {
            // Источники: http://www.apsalin.com/convert-geodetic-to-cartesian.aspx + https://www.calculatorsoup.com/calculators/geometry-solids/distance-two-points.php (по прямой) ;
            // https://planetcalc.ru/ (с учётом кривизны).


            // Координата первой точки.
            coord1 = new UIMap.Coordinate(
                1,
                2,
                3,
                0,
                0,
                0,
                false,
                false
            );
            // Координата второй точки.
            coord2 = new UIMap.Coordinate(
                1,
                2,
                3,
                0,
                0,
                0,
                true,
                true
            );
            // Ожидаемое расстояние по прямой.
            expStreight = 0;
            // Ожидаемое расстояние по дуге.
            expCurve = 0;


            // Выбор метода записи координат.
            radian = true;
        }

        /// <summary>
        /// Запускается перед каждым тестом.
        /// </summary>
        [TestInitialize]
        public void StartApp()
        {
            uiMap.LaunchApp();

            if (!radian)
            {
                DistanceRecord = DistanceXyzRecorded;
                DistanceStringRecord = DistanceXyzStringRecorded;
            }
            else
            {
                DistanceRecord = DistanceLlhRecorded;
                DistanceStringRecord = DistanceLlhStringRecorded;
            }
        }

        /// <summary>
        /// Запускается после каждого теста.
        /// </summary>
        [TestCleanup]
        public void CloseApp()
        {
            // Закрытие программы через (Alt + F4).
            Keyboard.SendKeys(uiMap.UITransformationofGeodWindow, "{F4}", ModifierKeys.Alt);
        }

        /// <summary>
        /// Ввод данных XYZ для расчёта расстояния.
        /// </summary>
        /// <param name="numAction">Выбор действия: 1 - с учётом кривизны, 2 - по прямой.</param>
        public void DistanceXyzRecorded(int numAction)
        {
            uiMap.SelAction(numAction);

            uiMap.SelectFirstPoint();
            uiMap.FirstPointSetXyz(coord1.x.ToString(cultureInfo), coord1.y.ToString(cultureInfo), coord1.z.ToString(cultureInfo));
            uiMap.FirstPointSelPolarity(coord1.northLat, coord1.eastLong);

            uiMap.SelectSecondPoint();
            uiMap.SecondPointSetXyz(coord2.x.ToString(cultureInfo), coord2.y.ToString(cultureInfo), coord2.z.ToString(cultureInfo));
            uiMap.SecondPointSelPolarity(coord2.northLat, coord2.eastLong);

            uiMap.ClickCalculation();
        }
        /// <summary>
        /// Ввод данных XYZ в виде строки для расчёта расстояния.
        /// </summary>
        /// <param name="numAction">Выбор действия: 1 - с учётом кривизны, 2 - по прямой.</param>
        /// <param name="x1">X первой точки.</param>
        /// <param name="y1">Y первой точки.</param>
        /// <param name="z1">Z первой точки.</param>
        /// <param name="x2">X второй точки.</param>
        /// <param name="y2">Y второй точки.</param>
        /// <param name="z2">Z второй точки.</param>
        public void DistanceXyzStringRecorded(int numAction, string x1, string y1, string z1, string x2, string y2, string z2)
        {
            uiMap.SelAction(numAction);

            uiMap.SelectFirstPoint();
            uiMap.FirstPointSetXyz(x1, y1, z1);
            uiMap.FirstPointSelPolarity(coord1.northLat, coord1.eastLong);

            uiMap.SelectSecondPoint();
            uiMap.SecondPointSetXyz(x2, y2, z2);
            uiMap.SecondPointSelPolarity(coord2.northLat, coord2.eastLong);

            uiMap.ClickCalculation();
        }
        /// <summary>
        /// Ввод данных LatLngHeight для расчёта расстояния.
        /// </summary>
        /// <param name="numAction">Выбор действия: 1 - с учётом кривизны, 2 - по прямой.</param>
        public void DistanceLlhRecorded(int numAction)
        {
            uiMap.SelAction(numAction);

            uiMap.SelectFirstPoint();
            uiMap.FirstPointSetLlh(coord1.lat.ToString(CultureInfo.InvariantCulture), coord1.lng.ToString(CultureInfo.InvariantCulture), coord1.height.ToString(CultureInfo.InvariantCulture));
            uiMap.FirstPointSelPolarity(coord1.northLat, coord1.eastLong);

            uiMap.SelectSecondPoint();
            uiMap.SecondPointSetLlh(coord2.lat.ToString(CultureInfo.InvariantCulture), coord2.lng.ToString(CultureInfo.InvariantCulture), coord2.height.ToString(CultureInfo.InvariantCulture));
            uiMap.SecondPointSelPolarity(coord2.northLat, coord2.eastLong);

            uiMap.ClickCalculation();
        }
        /// <summary>
        /// Ввод данных LatLngHeight в виде строки для расчёта расстояния.
        /// </summary>
        /// <param name="numAction">Выбор действия: 1 - с учётом кривизны, 2 - по прямой.</param>
        /// <param name="lat1">Широта первой точки.</param>
        /// <param name="lon1">Долгота первой точки.</param>
        /// <param name="hgh1">Высота первой точки.</param>
        /// <param name="lat2">Широта второй точки.</param>
        /// <param name="lon2">Долгота второй точки.</param>
        /// <param name="hgh2">Высота второй точки.</param>
        public void DistanceLlhStringRecorded(int numAction, string lat1, string lon1, string hgh1, string lat2, string lon2, string hgh2)
        {
            uiMap.SelAction(numAction);

            uiMap.SelectFirstPoint();
            uiMap.FirstPointSetLlh(lat1, lon1, hgh1);
            uiMap.FirstPointSelPolarity(coord1.northLat, coord1.eastLong);

            uiMap.SelectSecondPoint();
            uiMap.SecondPointSetLlh(lat2, lon2, hgh2);
            uiMap.SecondPointSelPolarity(coord2.northLat, coord2.eastLong);

            uiMap.ClickCalculation();
        }

        /// <summary>
        /// Проверка значения расстояния в таблице при расчёте расстояния по прямой.
        /// </summary>
        [TestMethod]
        public void DistanceStraight()
        {
            DistanceRecord(2);

            var act = uiMap.GetCellResult();

            Debug.WriteLine($"Ожидается: <{expStreight}>. Фактически: <{act}>");
            Assert.AreEqual(expStreight, act, DELTA_STREIGHT);
        }
        /// <summary>
        /// Проверка значения расстояния в таблице при расчёте расстояния по прямой
        /// при записи строки.
        /// </summary>
        [TestMethod]
        public void DistanceStraightString()
        {
            DistanceStringRecord(2, "test1", "test2", "test3", "test4", "test5", "test6");

            var act = uiMap.GetCellResult();

            Debug.WriteLine($"Ожидается: <NaN>. Фактически: <{act}>");
            Assert.AreEqual("NaN", act.ToString());
        }

        /// <summary>
        /// Проверка значения расстояния в таблице при расчёте расстояния по дуге.
        /// </summary>
        [TestMethod]
        public void DistanceCurve()
        {
            DistanceRecord(1);

            var act = uiMap.GetCellResult();

            Debug.WriteLine($"Ожидается: <{expCurve}>. Фактически: <{act}>");
            Assert.AreEqual(expCurve, act, DELTA_CURVE);
        }
        /// <summary>
        /// Проверка значения расстояния в таблице при расчёте расстояния по дуге
        /// при записи строки.
        /// </summary>
        [TestMethod]
        public void DistanceCurveString()
        {
            DistanceStringRecord(1, "test1", "test2", "test3", "test4", "test5", "test6");

            var act = uiMap.GetCellResult();

            Debug.WriteLine($"Ожидается: <NaN>. Фактически: <{act}>");
            Assert.AreEqual("NaN", act.ToString());
        }
    }
}
