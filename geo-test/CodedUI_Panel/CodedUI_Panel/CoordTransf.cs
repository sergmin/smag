﻿using System.Windows.Input;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using System.Diagnostics;
using System.Globalization;


namespace CodedUI_Panel
{
    /// <summary>
    /// Класс для тестирования преобразования координат.
    /// </summary>
    [CodedUITest]
    public class CoordTransf
    {
        /// <summary>
        /// Тестируемая точка.
        /// </summary>
        private readonly UIMap.Coordinate coord;

        /// <summary>
        /// Если true, то вкладка "Первая точка", если false - вкладка "Вторая точка".
        /// </summary>
        private readonly bool firstPoint;

        /// <summary>
        /// Формирование чисел с плавающей точкой при записи.
        /// </summary>
        private readonly CultureInfo cultureInfo = CultureInfo.InvariantCulture;

        /// <summary>
        /// Дельта для радиан.
        /// </summary>
        private const double DELTA_RADIAN = 1e-6;

        /// <summary>
        /// Дельта для метров.
        /// </summary>
        private const double DELTA_METERS = 0.1;

        /// <summary>
        /// Получает или устанавливает контекст теста, в котором предоставляются
        /// сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        /// <summary>
        /// Получение объекта класса взаимодействия с элементами интерфейса пользователя.
        /// </summary>
        public UIMap UIMap => map ?? (map = new UIMap());
        private UIMap map;

        /// <summary>
        /// Тип делегата для получения координаты точки.
        /// </summary>
        /// <param name="x">Координата X или Lat.</param>
        /// <param name="y">Координата Y или Lng.</param>
        /// <param name="z">Координата Z или Height.</param>
        private delegate void GetCoord(out double x, out double y, out double z);

        /// <summary>
        /// Тип делегата для установки координаты точки.
        /// </summary>
        /// <param name="x">Координата X или Lat.</param>
        /// <param name="y">Координата Y или Lng.</param>
        /// <param name="z">Координата Z или Height.</param>
        private delegate void SetCoord(string x, string y, string z);

        /// <summary>
        /// Тип делегата для установки полярности координаты точки.
        /// </summary>
        /// <param name="lat">Если true, то северная широта, если false - южная.</param>
        /// <param name="lng">Если true, то восточная долгота, если false - западная.</param>
        private delegate void SetCoordPolarity(bool lat, bool lng);

        /// <summary>
        /// Делегаты для получения координаты точки.
        /// </summary>
        private GetCoord GetXyz, GetLlh, GetXyzTable, GetLlhTable;

        /// <summary>
        /// Делегаты для установки координаты точки.
        /// </summary>
        private SetCoord SetXyz, SetLlh;

        /// <summary>
        /// Делегат для установки полярности координаты точки.
        /// </summary>
        private SetCoordPolarity SetPolarity;

        /// <summary>
        /// Инициализация полей класса для тестирования.
        /// </summary>
        public CoordTransf()
        {
            // Источники: https://www.rapidtables.com/ + http://www.apsalin.com/convert-geodetic-to-cartesian.aspx.


            // Координата тестируемой точки.
            coord = new UIMap.Coordinate(
                6378136.9364,
                0.0372,
                0.0216,
                0,
                0,
                0,
                true,
                true
            );

            // Выбор вкладки.
            firstPoint = false;
        }

        /// <summary>
        /// Запускается перед каждым тестом.
        /// </summary>
        [TestInitialize]
        public void StartApp()
        {
            UIMap.LaunchApp();


            if (firstPoint)
            {
                GetXyz = UIMap.FirstPointGetXyz;
                GetLlh = UIMap.FirstPointGetLlh;
                SetXyz = UIMap.FirstPointSetXyz;
                SetLlh = UIMap.FirstPointSetLlh;
                SetPolarity = UIMap.FirstPointSelPolarity;
                GetXyzTable = UIMap.FirstPointGetXyzTable;
                GetLlhTable = UIMap.FirstPointGetLlhTable;

                UIMap.SelectFirstPoint();
            }
            else
            {
                GetXyz = UIMap.SecondPointGetXyz;
                GetLlh = UIMap.SecondPointGetLlh;
                SetXyz = UIMap.SecondPointSetXyz;
                SetLlh = UIMap.SecondPointSetLlh;
                SetPolarity = UIMap.SecondPointSelPolarity;
                GetXyzTable = UIMap.SecondPointGetXyzTable;
                GetLlhTable = UIMap.SecondPointGetLlhTable;

                UIMap.SelectSecondPoint();
            }
        }

        /// <summary>
        /// Запускается после каждого теста.
        /// </summary>
        [TestCleanup]
        public void CloseApp()
        {
            // Закрытие программы через (Alt + F4).
            Keyboard.SendKeys(UIMap.UITransformationofGeodWindow, "{F4}", ModifierKeys.Alt);
        }

        /// <summary>
        /// Ввод данных для преобразования XYZ в LatLngHeight.
        /// </summary>
        public void TransfXyzToLlhRecorded()
        {
            UIMap.SelAction(0);

            SetXyz(coord.x.ToString(cultureInfo), coord.y.ToString(cultureInfo), coord.z.ToString(cultureInfo));
            SetPolarity(coord.northLat, coord.eastLong);
            UIMap.ClickCalculation();
        }
        /// <summary>
        /// Ввод данных для преобразования LatLngHeight в XYZ.
        /// </summary>
        public void TransfLlhToXyzRecorded()
        {
            UIMap.SelAction(0);

            SetLlh(coord.lat.ToString(cultureInfo), coord.lng.ToString(cultureInfo), coord.height.ToString(cultureInfo));
            SetPolarity(coord.northLat, coord.eastLong);
            UIMap.ClickCalculation();
        }


        /// <summary>
        /// Проверка значения широты в текстбоксе (вкладка) при преобразовании координаты XYZ в LatLngHeight.
        /// </summary>
        [TestMethod]
        public void XyzToLlhLat()
        {
            TransfXyzToLlhRecorded();

            GetLlh(out var actLat, out var actLong, out var actHeight);

            Debug.WriteLine($"Ожидается: <{coord.lat}>. Фактически: <{actLat}>");
            Assert.AreEqual(coord.lat, actLat, DELTA_RADIAN);
        }
        /// <summary>
        /// Проверка значения долготы в текстбоксе (вкладка) при преобразовании координаты XYZ в LatLngHeight.
        /// </summary>
        [TestMethod]
        public void XyzToLlhLong()
        {
            TransfXyzToLlhRecorded();

            GetLlh(out var actLat, out var actLong, out var actHeight);

            Debug.WriteLine($"Ожидается: <{coord.lng}>. Фактически: <{actLong}>");
            Assert.AreEqual(coord.lng, actLong, DELTA_RADIAN);
        }
        /// <summary>
        /// Проверка значения высоты в текстбоксе (вкладка) при преобразовании координаты XYZ в LatLngHeight.
        /// </summary>
        [TestMethod]
        public void XyzToLlhHeight()
        {
            TransfXyzToLlhRecorded();

            GetLlh(out var actLat, out var actLong, out var actHeight);

            Debug.WriteLine($"Ожидается: <{coord.height}>. Фактически: <{actHeight}>");
            Assert.AreEqual(coord.height, actHeight, DELTA_METERS);
        }
        /// <summary>
        /// Проверка значения в текстбоксе (вкладка) при преобразовании координаты XYZ в LatLngHeight
        /// при записи строки.
        /// </summary>
        [TestMethod]
        public void XyzToLlhString()
        {
            UIMap.SelAction(0);

            SetXyz("coordX", "coordY", "coordZ");
            SetPolarity(coord.northLat, coord.eastLong);
            UIMap.ClickCalculation();

            GetLlhTable(out var actLat, out var actLong, out var actHeight);

            Debug.WriteLine($"Ожидается: <NaN>. Фактически: <{actLat}>");
            Debug.WriteLine($"Ожидается: <NaN>. Фактически: <{actLong}>");
            Debug.WriteLine($"Ожидается: <NaN>. Фактически: <{actHeight}>");
            Assert.AreEqual("NaN", actLat.ToString());
            Assert.AreEqual("NaN", actLong.ToString());
            Assert.AreEqual("NaN", actHeight.ToString());
        }

        /// <summary>
        /// Проверка значения X в текстбоксе (вкладка) при преобразовании координаты LatLngHeight в XYZ.
        /// </summary>
        [TestMethod]
        public void LlhToXyzX()
        {
            TransfLlhToXyzRecorded();

            GetXyz(out var actX, out var actY, out var actZ);

            Debug.WriteLine($"Ожидается: <{coord.x}>. Фактически: <{actX}>");
            Assert.AreEqual(coord.x, actX, DELTA_METERS);
        }
        /// <summary>
        /// Проверка значения Y в текстбоксе (вкладка) при преобразовании координаты LatLngHeight в XYZ.
        /// </summary>
        [TestMethod]
        public void LlhToXyzY()
        {
            TransfLlhToXyzRecorded();

            GetXyz(out var actX, out var actY, out var actZ);

            Debug.WriteLine($"Ожидается: <{coord.y}>. Фактически: <{actY}>");
            Assert.AreEqual(coord.y, actY, DELTA_METERS);
        }
        /// <summary>
        /// Проверка значения Z в текстбоксе (вкладка) при преобразовании координаты LatLngHeight в XYZ.
        /// </summary>
        [TestMethod]
        public void LlhToXyzZ()
        {
            TransfLlhToXyzRecorded();

            GetXyz(out var actX, out var actY, out var actZ);

            Debug.WriteLine($"Ожидается: <{coord.z}>. Фактически: <{actZ}>");
            Assert.AreEqual(coord.z, actZ, DELTA_METERS);
        }
        /// <summary>
        /// Проверка значения в текстбоксе (вкладка) при преобразовании координаты LatLngHeight в XYZ
        /// при записи строки.
        /// </summary>
        [TestMethod]
        public void LlhToXyzString()
        {
            UIMap.SelAction(0);

            SetLlh("lat", "long", "height");
            SetPolarity(coord.northLat, coord.eastLong);
            UIMap.ClickCalculation();

            GetXyzTable(out var actX, out var actY, out var actZ);

            Debug.WriteLine($"Ожидается: <NaN>. Фактически: <{actX}>");
            Debug.WriteLine($"Ожидается: <NaN>. Фактически: <{actY}>");
            Debug.WriteLine($"Ожидается: <NaN>. Фактически: <{actZ}>");
            Assert.AreEqual("NaN", actX.ToString());
            Assert.AreEqual("NaN", actY.ToString());
            Assert.AreEqual("NaN", actZ.ToString());
        }

        /// <summary>
        /// Проверка значения широты в таблице при преобразовании координаты XYZ в LatLngHeight.
        /// </summary>
        [TestMethod]
        public void XyzToLlhTableLat()
        {
            TransfXyzToLlhRecorded();

            GetLlhTable(out var actLat, out var actLong, out var actHeight);

            Debug.WriteLine($"Ожидается: <{coord.lat}>. Фактически: <{actLat}>");
            Assert.AreEqual(coord.lat, actLat, DELTA_RADIAN);
        }
        /// <summary>
        /// Проверка значения долготы в таблице при преобразовании координаты XYZ в LatLngHeight.
        /// </summary>
        [TestMethod]
        public void XyzToLlhTableLong()
        {
            TransfXyzToLlhRecorded();

            GetLlhTable(out var actLat, out var actLong, out var actHeight);

            Debug.WriteLine($"Ожидается: <{coord.lng}>. Фактически: <{actLong}>");
            Assert.AreEqual(coord.lng, actLong, DELTA_RADIAN);
        }
        /// <summary>
        /// Проверка значения высоты в таблице при преобразовании координаты XYZ в LatLngHeight.
        /// </summary>
        [TestMethod]
        public void XyzToLlhTableHeight()
        {
            TransfXyzToLlhRecorded();

            GetLlhTable(out var actLat, out var actLong, out var actHeight);

            Debug.WriteLine($"Ожидается: <{coord.height}>. Фактически: <{actHeight}>");
            Assert.AreEqual(coord.height, actHeight, DELTA_METERS);
        }


        /// <summary>
        /// Проверка значения X в таблице при преобразовании координаты LatLngHeight в XYZ.
        /// </summary>
        [TestMethod]
        public void LlhToXyzTableX()
        {
            TransfLlhToXyzRecorded();

            GetXyzTable(out var actX, out var actY, out var actZ);

            Debug.WriteLine($"Ожидается: <{coord.x}>. Фактически: <{actX}>");
            Assert.AreEqual(coord.x, actX, DELTA_METERS);
        }
        /// <summary>
        /// Проверка значения Y в таблице при преобразовании координаты LatLngHeight в XYZ.
        /// </summary>
        [TestMethod]
        public void LlhToXyzTableY()
        {
            TransfLlhToXyzRecorded();

            GetXyzTable(out var actX, out var actY, out var actZ);

            Debug.WriteLine($"Ожидается: <{coord.y}>. Фактически: <{actY}>");
            Assert.AreEqual(coord.y, actY, DELTA_METERS);
        }
        /// <summary>
        /// Проверка значения Z в таблице при преобразовании координаты LatLngHeight в XYZ.
        /// </summary>
        [TestMethod]
        public void LlhToXyzTableZ()
        {
            TransfLlhToXyzRecorded();

            GetXyzTable(out var actX, out var actY, out var actZ);

            Debug.WriteLine($"Ожидается: <{coord.z}>. Фактически: <{actZ}>");
            Assert.AreEqual(coord.z, actZ, DELTA_METERS);
        }
    }
}
